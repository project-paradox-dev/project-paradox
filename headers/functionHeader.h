#ifndef FUNCTIONHEADER
#define FUNCTIONHEADER
/*
#define WINDOW_WIDTH (1280)
#define WINDOW_HEIGHT (720)
#define BPP 24
#define SCROLL_SPEED (300)
#define SPEED (300)
#define FRAMES (60)
*/

#define BPP 24
#define SCROLL_SPEED (300)
#define SPEED (300)
#define FRAMES (60)
#define MAX_PLAYERS (2)
//variable global para cerrar ventana
extern int close_requested;

typedef struct Player {
    float x_vel;
    float y_vel;
    float x_pos;
    float y_pos;
    int id;
    int frame[5];
    int width;
    int height;
    int flip;

}player;

typedef struct Objects {

    int id;
    int width;
    int height;
    float x_pos;
    float y_pos;

} Objects;


//definir estructura de datos para los controles de los jugadores
typedef struct Controls {
    int up;
    int down;
    int left;
    int right;
    int action;
}ctrls;

//pasamos los punteros a la funcion, permitiendonos cambiar los valores dinamicamente 
void eventListener( ctrls *P1ptr,  ctrls *P2ptr);
void loadMenu();
int menuListener();
int loadLevel(int lvl);
void countdown(int num);
int startGamePlaceholder();
int genRandomNumber(int min, int max);

void createPlayer(player* playerPTR, int playerNumber);
void playerMovement(ctrls* ctrlPTR, player* playerPTR);

void playerAreaCollision(player* playerPTR, int divisorId, int floorId, int playerNumber);

int playerObjectCollision(player* playerPTR, int objectID);

//desactiva todos los frames y solo activa el que se le pide. sprite-> el jugador, frameQty -> la cantidad de frames -1, activeFrame -> posicion en el array de frames que se quiere activar.
void playerAnimation(player* sprite, int frameQty, int activeFrame);
void drawWinner(int winner);

//struct createImage(int x, int y, int w, int h, char src[]);

#endif