#ifndef IMAGE_H
#define IMAGE_H
#include "SDL.h"
#define MAX_IMG 100

typedef struct Img
{
	int id;
	SDL_Surface* image;
	SDL_Texture* texture;
	SDL_Rect dest;  //destino y rectangulo de hitbox de la imagen
					//donde se va a dibujar la imagen 
	int flip;
	int active;
}IMG;
IMG imageArray[MAX_IMG];

int  loadImage(char* fileName);
void moveImage(int numImg, int x, int y);

void drawAllImages(void);
void deleteImage(int id);
void deleteAllImages();
int findImageById(int id);
void toggleActive(int id, int toggle);
void toggleFlip(int id, int toggle);


#endif
