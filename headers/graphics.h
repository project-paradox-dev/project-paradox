#ifndef GRAPHICS_H
#define GRAPHICS_H
#include "SDL.h"

#define WINDOW_WIDTH (1280)
#define WINDOW_HEIGHT (720)


int initSDL();
void endSDL();

void clearWindow();
void updateWindow();

int drawImage(SDL_Texture* texture, SDL_Rect* pDest, int flip);
SDL_Renderer* getRenderer(void);


#endif