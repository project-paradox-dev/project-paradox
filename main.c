#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>


#include <SDL.h>
#include <SDL_timer.h>
#include <SDL_image.h>
#include "headers/image.h"
#include "headers/graphics.h"
#include "headers/audio.h"
#include "headers/functionHeader.h"
//#include <stdlib.h>

/*
   The duck knows you're reading the code. It beckons you closer.
      ,~~.       ______________________________________________________________
     (  8 )-_, <( There are 8 ducks hidden in the game. Can you find them all? )
(\___ )=='-'     --------------------------------------------------------------
 \ .   ) )
  \_`-'_/

*/

int close_requested = 0;
int gameStatus, countdownNum = 1, randomLevel;
int player1Points = 0, player2Points = 0, maxPoints = 5, maxRounds = 10;


int main(int argc, char* argv[]) {
   

    int menu = 0, round = 0;
    initSDL();


    //mientras que no se cierre el juego y los jugadores no excedan los puntos maximos, seguimos cargando niveles
    //todo esto mientras que no se acaba el tiempo
 
    //initAudio();
    while (!close_requested) {
        menu = 0;
      
        loadMenu();//cargamos el menu 
        while (menu == 0 && !close_requested) {//esperamos a que haga algo el menu
            menu = menuListener();
            drawAllImages();
            updateWindow();
            clearWindow();

        }

        //mientras que no se cierre el juego y los jugadores no excedan los puntos maximos, seguimos cargando niveles
        //todo esto mientras que no se acaba el tiempo
       
        while (menu == 1 && !close_requested && player1Points < maxPoints && player2Points < maxPoints && round < maxRounds) {

            deleteAllImages();
            updateWindow();
            clearWindow();
            while (countdownNum != 4) {
                countdown(countdownNum);
                drawAllImages();
                updateWindow();
                clearWindow();
                deleteAllImages();
                countdownNum++;
                SDL_Delay(300);
            }
            countdownNum = 1;

            randomLevel = genRandomNumber(1, 5);
            gameStatus = loadLevel(randomLevel);

            switch (gameStatus) {
            case 0:
                printf("Empate\n");
                break;
            case 1:
                player1Points++;
                printf("Player 1 gana, %d puntos\n", player1Points);
                break;
            case 2:
                player2Points++;
                printf("Player 2 gana, %d puntos\n", player2Points);
                break;
            }
            printf("%d return de loadLevel\n", gameStatus);

            SDL_Delay(50);


            //audioTerminate();
            
            printf("Game Over\n");
        }

        if (player1Points > player2Points) drawWinner(1);
        else if (player2Points > player1Points) drawWinner(2);
        else if (player2Points == player1Points && round !=0) drawWinner(3);
        player1Points = 0;
        player2Points = 0;

        if (menu == 2) {
            deleteAllImages();
            updateWindow();
            clearWindow();
           menu = controls();
        }
       
    }
    endSDL();
    SDL_Delay(5000);
    return 0;
}
