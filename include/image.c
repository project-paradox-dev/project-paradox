#include "SDL.h"
#include "SDL_image.h"

//#include "graphics.h"
#include "../headers/image.h"
#include <stdio.h>
#include "../headers/graphics.h"




int imageQty = 0;
int id = 0;




int loadImage(char* fileName)
{
	
	SDL_Surface* surface = IMG_Load(fileName);//surface es como se representa una imagen en memoria

		//-1 indica que no usamos ningun driver grafico especifico
	SDL_Renderer* gRenderer = getRenderer();

	if (imageQty < MAX_IMG)
	{
		//surface = SDL_LoadBMP(fileName);
		if (surface == NULL)
		{
			fprintf(stderr, "Ezin da kargatu %s: %s\n", fileName, SDL_GetError());
			return -1;
		}
		else
		{
			//colorkey = SDL_MapRGB(surface->format, 255, 0, 255);
			//SDL_SetColorKey(surface, SDL_TRUE, colorkey);
			imageArray[imageQty].image = surface;
			imageArray[imageQty].texture = SDL_CreateTextureFromSurface(gRenderer, surface); //pasamos la imagen que hemos cargado en memoria a 
			imageArray[imageQty].dest.x = imageArray[imageQty].dest.y = 0;
			imageArray[imageQty].dest.w = surface->w;
			imageArray[imageQty].dest.h = surface->h;
			SDL_FreeSurface(surface); //liberamos la imagen de memoria
			imageArray[imageQty].id = id;
			imageArray[imageQty].active = 1;
			imageArray[imageQty].flip = 0;
			imageQty++;
			id++;
		}
	}
	else
	{
		printf("Irudien kopuru maximoa gainditu da.\n");
		return -1;
	}

	return id - 1;
}



void moveImage(int numImg, int x, int y)
{

	int id;

	id = findImageById(numImg);

	imageArray[id].dest.x = x;
	imageArray[id].dest.y = y;
}



void drawAllImages(void)
{
	int i;

	for (i = 0; i < imageQty; i++)
	{
		if (imageArray[i].active) {
			drawImage(imageArray[i].texture, &imageArray[i].dest, imageArray[i].flip);
		}
	}
}


void deleteImage(int id)
{
	int i, pos = 0;

	pos = findImageById(id);
	SDL_DestroyTexture(imageArray[pos].texture);
	for (i = pos; i < imageQty; i++)
	{
		imageArray[i] = imageArray[i + 1];
	}
	imageQty--;
}
void deleteAllImages() {
	int i = 0;
	int imageQtyTMP = imageQty;
	/*
	for (i = 0; i < imageQty; i++)
	{
		SDL_DestroyTexture(imageArray[i].texture);
		imageArray[i] = imageArray[i + 1];
		imageQty--;
	}
	*/
	
	for (i = 0; i < imageQtyTMP; i++)
	{
		deleteImage(i);
	}
	id = 0;
}

int findImageById(int id)
{
	int i = 0;
	
	for (i = 0; i < imageQty; i++)
	{
		if (id == imageArray[i].id) return i;
	}
	return -1;
}

void toggleActive(int id, int toggle) {
	
	imageArray[id].active = toggle;
	
}

void toggleFlip(int id, int toggle) {

	imageArray[id].flip = toggle;

}
/*int animation(int vel_x, int vel_y, SDL_RendererFlip flip) {

	switch (vel_x && vel_y) {
	case vel_x < 0: //el frame se mantiene solo cuando tiene velocidad
		flip = SDL_FLIP_HORIZONTAL;
		break;
	case vel_x > 0:
		flip = SDL_FLIP_NONE;
		break;
	case  vel_y < 0:

		break;
	case  vel_y > 0:

		break;
	}
	break;

	return flip;
}*/
