#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include "../headers/functionHeader.h"

#include <stdlib.h>
#include <time.h>

#include <SDL.h>
#include <SDL_timer.h>
#include <SDL_image.h>
#include "../headers/image.h"
#include "../headers/graphics.h"
//#include "../main.c"

// funtzio honek ausazko zenbakiak itzultzen ditu:
// lower = zenbaki txikiena
// upper = zenbaki handiena
int printRandoms(int lower, int upper)
{
    int num;

    num = (rand() % (upper - lower + 1)) + lower;

    return num;
}

int startGameFreeFall(void)
{
    //creamos un puntero para el player 1 y su estructura de datos
    ctrls* P1ptr, P1;
    //inicializamos los valores de la estructura de datos 
    P1.up = 0;
    P1.down = 0;
    P1.left = 0;
    P1.right = 0;
    P1.action = 0;

    //asignamos los valores de la estructura de dato al puntero, y cuando 
    //queramos cambiar los valores de P1 desde fuera, usamos el puntero
    P1ptr = &P1;

    //lo mismo pero para P2 (player 2)
    ctrls* P2ptr, P2;

    P2.up = 0;
    P2.down = 0;
    P2.left = 0;
    P2.right = 0;
    P2.action = 0;

    P2ptr = &P2;


    //======== Background ========
    int background;
    background = loadImage("project-paradox/backgrounds/level3BG.png");

    //========Floor & Divisor ========

    int floor;
    floor = loadImage("project-paradox/sprites/floor.png");
    moveImage(floor, 0, WINDOW_HEIGHT - imageArray[floor].dest.h);
    drawImage(imageArray[floor].texture, &imageArray[floor].dest, 0);

    int divisor;
    divisor = loadImage("project-paradox/sprites/divisor.png");
    moveImage(divisor, WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2, 0);
    drawImage(imageArray[divisor].texture, &imageArray[divisor].dest, 0);

    //========Player1========
    player* player1PTR, player1;
    player1PTR = &player1;
    createPlayer(player1PTR, 1);

    //========Player2========
    player* player2PTR, player2;
    player2PTR = &player2;
    createPlayer(player2PTR, 2);
    player2.x_pos = WINDOW_WIDTH - player2.width;
    moveImage(player2.id, player2.x_pos, 0);

    //=============================================================================================================

    int stop3 = 0;
    int winner3 = 0;

    int coinNumber1 = 0;
    int coinNumber2 = 0;
    int coinPlaying1[3];
    int coinPlaying2[3];
    int coinCounter1 = 0;
    int coinCounter2 = 0;
    int coinTimer = 3000;

    int tmp = 0; int i;

    int startObs[6];
    int obstacleTimer = 2000;
    int obstacleSequence[6];
    int obsSequencePos;
    int minObs = 1, maxObs = 6;

    int obsSpeed[6]; int obsSpeedSeqPos;
    int minVel = 150; int maxVel = 350;

    //Oztopoak hasieratzen dira hemen

    Objects obstacles1[6];

    for (i = 0; i < 6; i++)
    {
        obstacles1[i].id = loadImage("project-paradox/sprites/blackHole1.png");
        obstacles1[i].y_pos = WINDOW_HEIGHT - imageArray[floor].dest.h;
        obstacles1[i].width = imageArray[obstacles1[i].id].dest.w;
        obstacles1[i].height = imageArray[obstacles1[i].id].dest.h;
    }
    obstacles1[0].x_pos = 0;
    obstacles1[1].x_pos = ((((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) / 6) * 1);
    obstacles1[2].x_pos = ((((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) / 6) * 2);
    obstacles1[3].x_pos = ((((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) / 6) * 3);
    obstacles1[4].x_pos = ((((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) / 6) * 4);
    obstacles1[5].x_pos = ((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) - imageArray[obstacles1[5].id].dest.w;

    for (i = 0; i < 6; i++)
    {
        moveImage(obstacles1[i].id, obstacles1[i].x_pos, obstacles1[i].y_pos);
        drawImage(imageArray[obstacles1[i].id].texture, &imageArray[obstacles1[i].id].dest, 0);
    }

    Objects obstacles2[6];

    for (i = 0; i < 6; i++)
    {
        obstacles2[i].id = loadImage("project-paradox/sprites/blackHole1.png");
        obstacles2[i].y_pos = WINDOW_HEIGHT - imageArray[floor].dest.h;
        obstacles2[i].width = imageArray[obstacles2[i].id].dest.w;
        obstacles2[i].height = imageArray[obstacles2[i].id].dest.h;
    }
    obstacles2[0].x_pos = ((WINDOW_WIDTH + imageArray[divisor].dest.w) / 2);
    obstacles2[1].x_pos = ((WINDOW_WIDTH + imageArray[divisor].dest.w) / 2) + ((((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) / 6) * 1);
    obstacles2[2].x_pos = ((WINDOW_WIDTH + imageArray[divisor].dest.w) / 2) + ((((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) / 6) * 2);
    obstacles2[3].x_pos = ((WINDOW_WIDTH + imageArray[divisor].dest.w) / 2) + ((((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) / 6) * 3);
    obstacles2[4].x_pos = ((WINDOW_WIDTH + imageArray[divisor].dest.w) / 2) + ((((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) / 6) * 4);
    obstacles2[5].x_pos = WINDOW_WIDTH - imageArray[obstacles2[5].id].dest.w;

    for (i = 0; i < 6; i++)
    {
        moveImage(obstacles2[i].id, obstacles2[i].x_pos, obstacles2[i].y_pos);
        drawImage(imageArray[obstacles2[i].id].texture, &imageArray[obstacles2[i].id].dest, 0);
    }

    //txanponak hasieratzen dira hemen

    Objects coinArray1[3];

    for (i = 0; i < 3; i++)
    {

        coinArray1[i].id = loadImage("project-paradox/sprites/coin.png");
        coinArray1[i].x_pos = 0;
        coinArray1[i].y_pos = WINDOW_HEIGHT - imageArray[floor].dest.h;
        coinArray1[i].width = imageArray[coinArray1[i].id].dest.w;
        coinArray1[i].height = imageArray[coinArray1[i].id].dest.h;
        imageArray[coinArray1[i].id].active = 0;
        moveImage(coinArray1[i].id, coinArray1[i].x_pos, coinArray1[i].y_pos);
        //drawAllImages();
        drawImage(imageArray[coinArray1[i].id].texture, &imageArray[coinArray1[i].id].dest, 0);
    }

    Objects coinArray2[3];

    for (i = 0; i < 3; i++)
    {

        coinArray2[i].id = loadImage("project-paradox/sprites/coin.png");
        coinArray2[i].x_pos = 0;
        coinArray2[i].y_pos = WINDOW_HEIGHT - imageArray[floor].dest.h;
        coinArray2[i].width = imageArray[coinArray2[i].id].dest.w;
        coinArray2[i].height = imageArray[coinArray2[i].id].dest.h;
        imageArray[coinArray2[i].id].active = 0;
        moveImage(coinArray2[i].id, coinArray2[i].x_pos, coinArray2[i].y_pos);
        //drawAllImages();
        drawImage(imageArray[coinArray2[i].id].texture, &imageArray[coinArray2[i].id].dest, 0);
    }

    //bigarren zoru bat ezartzen da

    int floor2;
    floor2 = loadImage("project-paradox/sprites/floor.png");
    moveImage(floor2, 0, WINDOW_HEIGHT - imageArray[floor2].dest.h);
    drawImage(imageArray[floor2].texture, &imageArray[floor2].dest, 0);

    //Ausazko erabilpenerako hazi bat ezartzen da

    srand(time(NULL));

    //Oztopoak zein ordenetan irtengo diren definitzen da ausaz; array batean gordetzen da hau

    for (obsSequencePos = 0; obsSequencePos != 6; obsSequencePos++)
    {
        tmp = printRandoms(minObs, maxObs);

        for (i = 0; i <= obsSequencePos; i++)
        {
            if (obstacleSequence[i] == tmp)
            {
                i = -1;
                tmp = printRandoms(minObs, maxObs);
            }
        }
        obstacleSequence[obsSequencePos] = tmp;

        //printf("%d ", obstacleSequence[ObsSequencePos]);
    }
    obsSequencePos = 0;

    //Oztopoen abiadurak definitzen dira ausaz; array batean gordetzen da hau

    for (obsSpeedSeqPos = 0; obsSpeedSeqPos != 6; obsSpeedSeqPos++)
    {
        tmp = printRandoms(minVel, maxVel);

        for (i = 0; i <= obsSpeedSeqPos; i++)
        {
            if (obsSpeed[i] == tmp)
            {
                i = -1;
                tmp = printRandoms(minVel, maxVel);
            }
        }
        obsSpeed[obsSpeedSeqPos] = tmp;
    }
    obsSpeedSeqPos = 0;

    while ((!close_requested) && (!stop3)) {

        //listeners 
        eventListener(P1ptr, P2ptr);

        //========Player1 Movement========
        playerMovement(P1ptr, player1PTR);
        //========Player1 Collisions========
        playerAreaCollision(player1PTR, divisor, floor, 1);
        playerUpdate(player1PTR);

        //========Player2 Movement========
        playerMovement(P2ptr, player2PTR);
        //========Player2 Collisions========
        playerAreaCollision(player2PTR, divisor, floor, 2);
        playerUpdate(player2PTR);

        //--------------------------------------------------------------------------------------------------------------------------------------      

        // Oztopoen ordenaren array-arekin eta denboraren arabera abiarazten dira oztopoak

        if ((obstacleTimer <= 0) && (stop3 == 0) && (obsSequencePos != 6))
        {
            obstacleTimer = 2000;

            obsSequencePos++;

            switch (obstacleSequence[obsSequencePos])
            {
            case 1:
                startObs[0] = 1;
                break;
            case 2:
                startObs[1] = 1;
                break;
            case 3:
                startObs[2] = 1;
                break;
            case 4:
                startObs[3] = 1;
                break;
            case 5:
                startObs[4] = 1;
                break;
            case 6:
                startObs[5] = 1;
                break;
            }
        }

        // Txanponak abiarazita al dauden erakusten duen array-arekin eta denboraren arabera abiarazten dira txanponak

        if ((coinTimer <= 0) && (stop3 == 0) && (coinNumber1 < 4) && (coinNumber2 < 4) && (coinCounter1 < 3) && (coinCounter2 < 3))
        {
            coinTimer = 3000;

            switch (coinNumber1)
            {
            case 1:
                if (coinPlaying1[0] != 1)
                {
                    coinArray1[0].x_pos = printRandoms(0, (((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) - (imageArray[coinArray1[0].id].dest.w)));
                    coinArray1[0].y_pos = printRandoms(0, WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[coinArray1[0].id].dest.h);
                    imageArray[coinArray1[0].id].active = 1;
                    moveImage(coinArray1[0].id, coinArray1[0].x_pos, coinArray1[0].y_pos);
                    coinPlaying1[0] = 1;
                }
                break;
            case 2:
                if (coinPlaying1[1] != 1)
                {
                    coinArray1[1].x_pos = printRandoms(0, ((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) - imageArray[coinArray1[1].id].dest.w);
                    coinArray1[1].y_pos = printRandoms(0, WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[coinArray1[1].id].dest.h);
                    imageArray[coinArray1[1].id].active = 1;
                    moveImage(coinArray1[1].id, coinArray1[1].x_pos, coinArray1[1].y_pos);
                    coinPlaying1[1] = 1;
                }
                break;
            case 3:
                if (coinPlaying1[2] != 1)
                {
                    coinArray1[2].x_pos = printRandoms(0, ((WINDOW_WIDTH - imageArray[divisor].dest.w) / 2) - (imageArray[coinArray1[2].id].dest.w));
                    coinArray1[2].y_pos = printRandoms(0, WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[coinArray1[2].id].dest.h);
                    imageArray[coinArray1[2].id].active = 1;
                    moveImage(coinArray1[2].id, coinArray1[2].x_pos, coinArray1[2].y_pos);
                    coinPlaying1[2] = 1;
                }
                break;
            }
            coinNumber1++;
            if (coinNumber1 > 3)
            {
                coinNumber1 = 1;
            }

            switch (coinNumber2)
            {
            case 1:
                if (coinPlaying2[0] != 1)
                {
                    coinArray2[0].x_pos = printRandoms(((WINDOW_WIDTH + imageArray[divisor].dest.w) / 2), WINDOW_WIDTH - imageArray[coinArray2[0].id].dest.w);
                    coinArray2[0].y_pos = printRandoms(0, WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[coinArray2[0].id].dest.h);
                    imageArray[coinArray2[0].id].active = 1;
                    moveImage(coinArray2[0].id, coinArray2[0].x_pos, coinArray2[0].y_pos);
                    coinPlaying2[0] = 1;
                }
                break;
            case 2:
                if (coinPlaying2[1] != 1)
                {
                    coinArray2[1].x_pos = printRandoms(((WINDOW_WIDTH + imageArray[divisor].dest.w) / 2), WINDOW_WIDTH - imageArray[coinArray2[1].id].dest.w);
                    coinArray2[1].y_pos = printRandoms(0, WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[coinArray2[1].id].dest.h);
                    imageArray[coinArray2[1].id].active = 1;
                    moveImage(coinArray2[1].id, coinArray2[1].x_pos, coinArray2[1].y_pos);
                    coinPlaying2[1] = 1;
                }
                break;
            case 3:
                if (coinPlaying2[2] != 1)
                {
                    coinArray2[2].x_pos = printRandoms(((WINDOW_WIDTH + imageArray[divisor].dest.w) / 2), WINDOW_WIDTH - imageArray[coinArray2[2].id].dest.w);
                    coinArray2[2].y_pos = printRandoms(0, WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[coinArray2[2].id].dest.h);
                    imageArray[coinArray2[2].id].active = 1;
                    moveImage(coinArray2[2].id, coinArray2[2].x_pos, coinArray2[2].y_pos);
                    coinPlaying2[2] = 1;
                }
                break;
            }
            coinNumber2++;
            if (coinNumber2 > 3)
            {
                coinNumber2 = 1;
            }
        }

        //oztopoen mugimendua

        for (i = 0; i < 6; i++)
        {
            if ((stop3 == 0) && (startObs[i] == 1))
            {
                obstacles1[i].y_pos -= obsSpeed[i] / FRAMES;
                moveImage(obstacles1[i].id, obstacles1[i].x_pos, obstacles1[i].y_pos);

                obstacles2[i].y_pos -= obsSpeed[i] / FRAMES;
                moveImage(obstacles2[i].id, obstacles2[i].x_pos, obstacles2[i].y_pos);
            }
        }

        //=============Collisions of the 2 players==========

        //========Obstacle 1 Collisions========

        for (i = 0; i < 6; i++)
        {
            if (((player1.x_pos <= obstacles1[i].x_pos + obstacles1[i].width) && (player1.x_pos + player1.width >= obstacles1[i].x_pos)) && ((player1.y_pos + player1.height >= obstacles1[i].y_pos) && (player1.y_pos <= obstacles1[i].y_pos + obstacles1[i].height))) {//COLISION obstaculo CON EL jugador DESDE los lados
                stop3 = 1;
                startObs[i] = 0;
                winner3 = 2;
            }

            if (((player2.x_pos <= obstacles2[i].x_pos + obstacles2[i].width) && (player2.x_pos + player2.width >= obstacles2[i].x_pos)) && ((player2.y_pos + player2.height >= obstacles2[i].y_pos) && (player2.y_pos <= obstacles2[i].y_pos + obstacles2[i].height))) {//COLISION obstaculo CON EL jugador DESDE los lados
                stop3 = 1;
                startObs[i] = 0;
                winner3 = 1;
            }
        }

        //========Coins collitions of the 2 players======

        for (i = 0; i < 3; i++)
        {
            if (imageArray[coinArray1[i].id].active == 1 && ((player1.x_pos <= coinArray1[i].x_pos + coinArray1[i].width) && (player1.x_pos + player1.width >= coinArray1[i].x_pos)) && ((player1.y_pos + player1.height >= coinArray1[i].y_pos) && (player1.y_pos <= coinArray1[i].y_pos + coinArray1[i].height))) {
                coinArray1[i].y_pos = WINDOW_HEIGHT - imageArray[floor].dest.h - 1;
                moveImage(coinArray1[i].id, coinArray1[i].x_pos, coinArray1[i].y_pos);
                coinPlaying1[i] = 0;
                coinNumber1 = 1;
                coinCounter1++;
            }

            if (imageArray[coinArray2[i].id].active == 1 && ((player2.x_pos <= coinArray2[i].x_pos + coinArray2[i].width) && (player2.x_pos + player2.width >= coinArray2[i].x_pos)) && ((player2.y_pos + player2.height >= coinArray2[i].y_pos) && (player2.y_pos <= coinArray2[i].y_pos + coinArray2[i].height))) {
                coinArray2[i].y_pos = WINDOW_HEIGHT - imageArray[floor].dest.h - 1;
                moveImage(coinArray2[i].id, coinArray2[i].x_pos, coinArray2[i].y_pos);
                coinPlaying2[i] = 0;
                coinNumber2 = 1;
                coinCounter2++;
            }
        }

        //=============================================================================================

        //oztopoak sabaia ukitzean lurrera itzultzen dira

        for (i = 0; i < 6; i++)
        {
            if (obstacles1[i].y_pos + obstacles1[i].height <= 0)
            {
                obstacles1[i].y_pos = WINDOW_HEIGHT - imageArray[floor].dest.h - 1;
                moveImage(obstacles1[i].id, obstacles1[i].x_pos, obstacles1[i].y_pos);

            }
            if (obstacles2[i].y_pos + obstacles2[i].height <= 0)
            {
                obstacles2[i].y_pos = WINDOW_HEIGHT - imageArray[floor].dest.h - 1;
                moveImage(obstacles2[i].id, obstacles2[i].x_pos, obstacles2[i].y_pos);

            }
        }

        drawAllImages();
        updateWindow();
        clearWindow();

        //denboraren aldagaien aldaketak:

        if (obstacleTimer != 0) {
            obstacleTimer = obstacleTimer - (1000 / FRAMES);
        }
        if (coinTimer != 0) {
            coinTimer = coinTimer - (1000 / FRAMES);
        }

        //esperamos para conseguir el framerate que queremos
        SDL_Delay(1000 / FRAMES);

        if (coinCounter1 >= 3)
        {
            stop3 = 1;
            winner3 = 1;
        }

        if (coinCounter2 >= 3)
        {
            stop3 = 1;
            winner3 = 2;
        }

    }

    //irabazteko adina puntu lortzean, jokalariak irabazi egin duela adieraziko du return zehatzen bidez; bestela, berdinketa zehaztuko da

    if (winner3 != 0)
    {
        return winner3;
    }
}