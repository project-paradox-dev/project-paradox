/*
"../headers/audio.h"
#include "SDL.h"
#include "SDL_mixer.h"
#include <stdio.h>
//#include <cstring>

void musicUnload(void);
void soundsUnload();

Mix_Chunk* sounds[MAX_SOUNDS];
int soundQty = 0;
Mix_Music* music = NULL;

void initAudio()
{
    if (Mix_OpenAudio(44100, MIX_INIT_MP3, 2, 2048) < 0)
    {
        printf("Ezin da SDL Mixer hasieratu, SDL_mixer Error: %s\n", Mix_GetError());
    }
    printf("Audioa hasieratu da!\n");
}

int loadSound(char* fileName)
{
    if (soundQty == MAX_SOUNDS) return -1;
    if ((sounds[soundQty] = Mix_LoadWAV(fileName)) == NULL)
    {
        printf("Failed to load low sound effect! SDL_mixer Error: %s\n", Mix_GetError());
        return -1;
    }
    soundQty++;
    return soundQty - 1;
}

int loadMusic(char* fileName)
{
    int wasPlaying = 0;

    if (music != NULL)
    {
        wasPlaying = (Mix_PlayingMusic() != 1);
        Mix_HaltMusic();
        //Mix_FreeMusic(musika);
    }
    if ((music = Mix_LoadMUS(fileName)) == NULL) return 0;
    if (wasPlaying)  Mix_PlayMusic(music, -1);
    return 1;
}

int playSound(int idSound)
{
    if ((idSound <= 0) && (idSound >= soundQty)) return -1;
    Mix_PlayChannel(0, sounds[idSound], 0);
    return idSound;
}

int playMusic(void)
{
    if (music != NULL)
    {
        Mix_PlayMusic(music, -1);
        return 1;
    }
    return 0;
}

void toggleMusic(void)
{
    if (music != NULL)
    {
        if (Mix_PlayingMusic() != 0)
        {
            if (Mix_PausedMusic()) Mix_ResumeMusic();
            else Mix_PauseMusic();
        }
        else playMusic();
    }
}

void musicUnload(void)
{
    if (music != NULL)
    {
        Mix_HaltMusic();
        Mix_FreeMusic(music);
    }
}

void soundsUnload()
{
    int i;

    for (i = 0; i < soundQty; i++)
    {
        Mix_FreeChunk(sounds[i]);
        sounds[i] = NULL;
    }
    soundQty = 0;
}

void audioTerminate(void)
{
    Mix_HaltChannel(-1);
    soundsUnload();
    musicUnload();
    Mix_Quit();
}
*/