#include <stdio.h>
#include <string.h>

#include <SDL.h>
#include <SDL_timer.h>
#include <SDL_image.h>

#include "../headers/image.h"
#include "../headers/graphics.h"
#include "../headers/functionHeader.h"


int startGameFallingBlocks() {
    //creamos un puntero para el player 1 y su estructura de datos
    ctrls* P1ptr, P1;
    //inicializamos los valores de la estructura de datos 
    P1.up = 0;
    P1.down = 0;
    P1.left = 0;
    P1.right = 0;
    P1.action = 0;

    //asignamos los valores de la estructura de dato al puntero, y cuando 
    //queramos cambiar los valores de P1 desde fuera, usamos el puntero
    P1ptr = &P1;

    //lo mismo pero para P2 (player 2)
    ctrls* P2ptr, P2;

    P2.up = 0;
    P2.down = 0;
    P2.left = 0;
    P2.right = 0;
    P2.action = 0;

    P2ptr = &P2;



    //======== Background ========
    int background;
    background = loadImage("project-paradox/backgrounds/level1BG.png");

    //========Floor & Divisor ========

    int floor;
    floor = loadImage("project-paradox/sprites/floor.png");
    moveImage(floor, 0, WINDOW_HEIGHT - imageArray[floor].dest.h);
    drawImage(imageArray[floor].texture, &imageArray[floor].dest, 0);

    int divisor;
    divisor = loadImage("project-paradox/sprites/divisor.png");
    moveImage(divisor, WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2, 0);
    drawImage(imageArray[divisor].texture, &imageArray[divisor].dest, 0);

    //Random
    srand(time(NULL));
    int fallCD;
    fallCD = 500;
    int fall = 0;
    int player1Death = 0;
    int player2Death = 0;
    int randomStage = genRandomNumber(0, 1);
    printf("random=%d", randomStage);
    //========Player1========
    //========Player1========
    player* player1PTR, player1;
    player1PTR = &player1;
    createPlayer(player1PTR, 1);
    if (randomStage == 0) {
        player1.x_pos = 0;
    }
    else if (randomStage == 1) {
        player1.x_pos = imageArray[divisor].dest.x - player1.width;
    }


    moveImage(player1.id, player1.x_pos, player1.y_pos);
    drawImage(imageArray[player1.id].texture, &imageArray[player1.id].dest, player1.flip);


    //========Player2========
    player* player2PTR, player2;
    player2PTR = &player2;
    createPlayer(player2PTR, 2);
    player2.x_pos = WINDOW_WIDTH - player2.width;
    moveImage(player2.id, player2.x_pos, 0);

    if (randomStage == 0) {
        player2.x_pos = WINDOW_WIDTH;
    }
    else if (randomStage == 1) {
        player2.x_pos = WINDOW_WIDTH / 2 + imageArray[divisor].dest.w / 2;
    }
    moveImage(player2.id, player2.x_pos, player2.y_pos);
    //drawAllImages();
    drawImage(imageArray[player2.id].texture, &imageArray[player2.id].dest, player2.flip);




    //Aitor juego


       //botones
    int button1;
    int button2;

    button1 = loadImage("project-paradox/sprites/orb.png");
    if (randomStage == 0) {
        moveImage(button1, WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2 - imageArray[button1].dest.w, WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[button1].dest.h);
    }
    else if (randomStage == 1) {
        moveImage(button1, 0, WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[button1].dest.h);
    }
    drawImage(imageArray[button1].texture, &imageArray[button1].dest, 0);
    button2 = loadImage("project-paradox/sprites/orb.png");
    if (randomStage == 0) {
        moveImage(button2, WINDOW_WIDTH / 2 + imageArray[divisor].dest.w / 2, WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[button2].dest.h);

    }
    else if (randomStage == 1) {
        moveImage(button2, WINDOW_WIDTH - imageArray[button2].dest.w, WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[button2].dest.h);

    }
    drawImage(imageArray[button2].texture, &imageArray[button2].dest, 0);

    int exitFallGame = 0;
    float cubeSpeed = 1000;
    int cubes[14];
    int pos_y_cubes[8] = { 0,0,0,0,0,0,0 };
    int i;
    for (i = 0; i < 14; i++) {
        cubes[i] = loadImage("project-paradox/sprites/asteroid.png");
    }
    if (randomStage == 0) {

        for (i = 0; i < 7; i++) {
            moveImage(cubes[i], imageArray[cubes[i]].dest.w * i, 0);
            drawImage(imageArray[cubes[i]].texture, &imageArray[cubes[i]].dest, 0);
        }
        for (i = 7; i < 14; i++) {
            moveImage(cubes[i], WINDOW_WIDTH - imageArray[cubes[i]].dest.w * (i - 6), 0);
            drawImage(imageArray[cubes[i]].texture, &imageArray[cubes[i]].dest, 0);
        }




    }
    else if (randomStage == 1) {
        for (i = 0; i < 7; i++) {
            moveImage(cubes[i], WINDOW_WIDTH - (imageArray[cubes[i]].dest.w * (7 - i)), 0);
            drawImage(imageArray[cubes[i]].texture, &imageArray[cubes[i]].dest, 0);
        }
        for (i = 7; i < 14; i++) {
            moveImage(cubes[i], WINDOW_WIDTH - imageArray[cubes[i]].dest.w * (i - 6) - player2.width, 0);
            drawImage(imageArray[cubes[i]].texture, &imageArray[cubes[i]].dest, 0);
        }



    }








    while ((!close_requested) && (exitFallGame == 0)) {
        //listeners 
        eventListener(P1ptr, P2ptr);
        //movimiento cubos con la generacion random de 50% para saber hacia que lado va a bajar
        if (randomStage == 0) {//1 es ----->
            if (fall >= 1) {
                pos_y_cubes[0] += cubeSpeed / FRAMES;
            }
            if (fall >= 2) {
                pos_y_cubes[1] += cubeSpeed / FRAMES;
            }
            if (fall >= 3) {
                pos_y_cubes[2] += cubeSpeed / FRAMES;
            }
            if (fall >= 4) {
                pos_y_cubes[3] += cubeSpeed / FRAMES;
            }
            if (fall >= 5) {
                pos_y_cubes[4] += cubeSpeed / FRAMES;
            }
            if (fall >= 6) {
                pos_y_cubes[5] += cubeSpeed / FRAMES;
            }
            if (fall >= 7) {
                pos_y_cubes[6] += cubeSpeed / FRAMES;
            }

        }
        if (randomStage == 1) {// 0 es <---
            if (fall >= 1) {
                pos_y_cubes[6] += cubeSpeed / FRAMES;
            }
            if (fall >= 2) {
                pos_y_cubes[5] += cubeSpeed / FRAMES;
            }
            if (fall >= 3) {
                pos_y_cubes[4] += cubeSpeed / FRAMES;
            }
            if (fall >= 4) {
                pos_y_cubes[3] += cubeSpeed / FRAMES;
            }
            if (fall >= 5) {
                pos_y_cubes[2] += cubeSpeed / FRAMES;
            }
            if (fall >= 6) {
                pos_y_cubes[1] += cubeSpeed / FRAMES;
            }
            if (fall >= 7) {
                pos_y_cubes[0] += cubeSpeed / FRAMES;
            }

        }





        //========Player1 Movement========
        playerMovement(P1ptr, player1PTR);
        //========Player1 Collisions========
        playerAreaCollision(player1PTR, divisor, floor, 1);
        playerUpdate(player1PTR);

        //========Player2 Movement========
        playerMovement(P2ptr, player2PTR);
        //========Player2 Collisions========
        playerAreaCollision(player2PTR, divisor, floor, 2);
        playerUpdate(player2PTR);





        //colission player1 boton
        if (randomStage == 0) {
            if (player1.x_pos + player1.width >= imageArray[button1].dest.x) {
                player1.x_pos = imageArray[button1].dest.x - player1.width;
            }
        }
        else if (randomStage == 1) {
            if (player1.x_pos <= imageArray[button1].dest.x + imageArray[button1].dest.w) {
                player1.x_pos = imageArray[button1].dest.x + imageArray[button1].dest.w;
            }
        }
        if (player1.y_pos <= imageArray[floor].dest.y - player1.height) {
            player1.y_pos = imageArray[floor].dest.y - player1.height;
        }

        //colision player2 boton
        if (randomStage == 0) {
            if (player2.x_pos <= imageArray[button2].dest.x + imageArray[button2].dest.w) {
                player2.x_pos = imageArray[button2].dest.x + imageArray[button2].dest.w;
            }
        }
        else if (randomStage == 1) {
            if (player2.x_pos + player2.width >= imageArray[button2].dest.x) {
                player2.x_pos = imageArray[button2].dest.x - player2.width;
            }
        }
        if (player2.y_pos <= imageArray[floor].dest.y - player2.height) {
            player2.y_pos = imageArray[floor].dest.y - player2.height;
        }
        //colision para hacer la pulsasion
        if (randomStage == 0) {
            if (player1.x_pos+player1.width >= imageArray[button1].dest.x - 5) {//player1 hitbox action
                if (P1.action && player2Death == 1) {
                    exitFallGame = 2;
                }
            }
            if (player2.x_pos <= imageArray[button2].dest.x + imageArray[button2].dest.w + 5) {//player2 hitbox action
                if (P2.action && player2Death == 0) {
                    exitFallGame = 3;


                }
            }
        }
        else if (randomStage == 1) {

            if (player2.x_pos + player2.width >= imageArray[button2].dest.x - 5) {//player2 hitbox action
                if (P2.action) {
                    return 2;


                }
            }
            if (player1.x_pos <= imageArray[button1].dest.x + imageArray[button1].dest.w + 5) {//player1 hitbox action
                if (P1.action) {
                    return 1;
                }
            }
        }


        //colission floor divisor
        if (player1.x_pos + player1.width >= imageArray[divisor].dest.x) {//COLISION PLAYER1 CON EL DIVISOR DESDE LA IZQUIERDA
            player1.x_pos = (WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2) - player1.width;
        }

        if (player1.y_pos + player1.height != WINDOW_HEIGHT - imageArray[floor].dest.h) {//COLISION PLAYER1 CON EL SUELO
            player1.y_pos = (WINDOW_HEIGHT - imageArray[floor].dest.h) - player1.height;
        }
        moveImage(player1.id, player1.x_pos, player1.y_pos);
        toggleFlip(player1.id, player1.flip);
        




        //colission floor divisor
        if (player2.x_pos <= imageArray[divisor].dest.x + imageArray[divisor].dest.w) {//COLISION PLAYER2 CON EL DIVISOR DESDE LA IZQUIERDA
            player2.x_pos = (WINDOW_WIDTH / 2 + imageArray[divisor].dest.w / 2);
        }

        if (player2.y_pos + player2.height != WINDOW_HEIGHT - imageArray[floor].dest.h) {//COLISION PLAYER2 CON EL SUELO
            player2.y_pos = (WINDOW_HEIGHT - imageArray[floor].dest.h) - player2.height;
        }
        moveImage(player2.id, player2.x_pos, player2.y_pos);
        toggleFlip(player2.id, player2.flip);
        /*
        //limpiamos la ventana
        SDL_RenderClear(gRenderer);
        */
        //y_pos -= (float)SCROLL_SPEED / FRAMES;
        //drawImage(imageArray[player1.id].texture, &imageArray[player1.id].dest, 0);
        //drawImage(imageArray[player2.id].texture, &imageArray[player2.id].dest, 0);


        //colisiones cubos con el suelo
        for (int i = 0; i < 7; i++) {
            if (colisionCubosSuelo(cubes[i], floor)) {
                pos_y_cubes[i] = imageArray[floor].dest.y - imageArray[cubes[i]].dest.h;
            }

        }

        //colisiones segunda pantalla cubos
        for (int i = 0; i < 7; i++) {
            if (colisionCubosSuelo(cubes[i + 7], floor)) {
                imageArray[cubes[i + 7]].dest.y = imageArray[floor].dest.y - imageArray[cubes[i + 7]].dest.h;
            }
        }

        //mover cubos
        if (randomStage == 0) {
            for (int i = 0; i < 7; i++) {
                moveImage(cubes[i], imageArray[cubes[i]].dest.w * i, pos_y_cubes[i]);
            }
            for (int i = 0; i < 7; i++) {
                moveImage(cubes[i + 7], WINDOW_WIDTH - imageArray[cubes[i + 7]].dest.w * (i + 1), pos_y_cubes[i]);
            }

        }
        else if (randomStage == 1) {
            for (int i = 0; i < 7; i++) {
                moveImage(cubes[i], imageArray[cubes[i]].dest.w * i + player1.width, pos_y_cubes[i]);
            }
            for (int i = 0; i < 7; i++) {
                moveImage(cubes[i + 7], WINDOW_WIDTH - imageArray[cubes[i + 7]].dest.w * (i + 1) - player1.width, pos_y_cubes[i]);
            }

        }





        i = 0;
        while ((player1Death == 0) && (i < 7)) {
            if (playerObjectCollision(player1PTR, cubes[i]) == 1) {

                player1Death = 1;

            }
            i++;
        }
        if (player1Death == 1) {
            toggleActive(player1.id, 0);
        }

        i = 0;
        while ((player2Death == 0) && (i < 7)) {
            if (playerObjectCollision(player2PTR, cubes[i+7]) == 1) {

                player2Death = 1;
            }
            i++;
        }

        if (player2Death == 1) {
            toggleActive(player2.id, 0);
        }
        if (player2Death == 1 && player1Death == 1) {
            exitFallGame = 1;
        }
        drawAllImages();
        updateWindow();
        clearWindow();


        if (fallCD > 0) {// esto es para que los cubos caigan cada tanto tiempo
            fallCD = fallCD - 1000 / FRAMES;// esto mira si no  esta en 0 y en este caso le baja al contador, esto hara llegue a 0

        }
        else if (fallCD <= 0) {//esto hara que caigan los bloques, cada vez que llegue a 0 empezara a caer un bloque y el timer subira a 250
            fallCD = 250;
            fall++;
           
        }



        //esperamos para conseguir el framerate que queremos
        SDL_Delay(1000 / FRAMES);
    }
    switch (exitFallGame) {
    case 1:
        return 0;
        break;
    case 2:
        return 1;
        break;
    case 3:
        return 2;
        break;

    }
}

int colisionCubosSuelo(int cubo, int suelo) {
    if (imageArray[cubo].dest.y + imageArray[cubo].dest.h >= imageArray[suelo].dest.y) {

        return 1;
    }
    return 0;
}