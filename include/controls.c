#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "../headers/functionHeader.h"


#include <SDL.h>
#include <SDL_timer.h>
#include <SDL_image.h>
#include "../headers/image.h"
#include "../headers/graphics.h"

int controls(void) {

    //creamos un puntero para el player 1 y su estructura de datos
    ctrls* P1ptr, P1;
    //inicializamos los valores de la estructura de datos 
    P1.up = 0;
    P1.down = 0;
    P1.left = 0;
    P1.right = 0;
    P1.action = 0;

    //asignamos los valores de la estructura de dato al puntero, y cuando 
    //queramos cambiar los valores de P1 desde fuera, usamos el puntero
    P1ptr = &P1;

    //lo mismo pero para P2 (player 2)
    ctrls* P2ptr, P2;

    P2.up = 0;
    P2.down = 0;
    P2.left = 0;
    P2.right = 0;
    P2.action = 0;

    P2ptr = &P2;
    

    int winner = 0;

    //variables para el tiempo
    int random = (rand() % (10 - 20 + 1)) + 10;
    printf("%d", random);
    //========Floor & Divisor ========
    int bg;
    bg = loadImage("project-paradox/backgrounds/controls.png");

    int floor;
    floor = loadImage("project-paradox/sprites/floor.png");
    moveImage(floor, 0, WINDOW_HEIGHT - imageArray[floor].image->h);
    drawImage(imageArray[floor].texture, &imageArray[floor].dest, 0);

    int divisor;
    divisor = loadImage("project-paradox/sprites/divisor.png");
    moveImage(divisor, WINDOW_WIDTH / 2 - imageArray[divisor].image->w / 2, 0);
    drawImage(imageArray[divisor].texture, &imageArray[divisor].dest, 0);

    //=========== key sprites ===========

    //p1
    int W, A, S, D, SPACE;
    W = loadImage("project-paradox/sprites/pressedW.png");
    A = loadImage("project-paradox/sprites/pressedA.png");
    S = loadImage("project-paradox/sprites/pressedS.png");
    D = loadImage("project-paradox/sprites/pressedD.png");
    SPACE = loadImage("project-paradox/sprites/pressedSPACE.png");
    toggleActive(W, 0);
    toggleActive(A, 0);
    toggleActive(S, 0);
    toggleActive(D, 0);
    toggleActive(SPACE, 0);
    moveImage(W, WINDOW_WIDTH/4 - imageArray[W].dest.w/2, WINDOW_HEIGHT/9 + imageArray[W].dest.h);
    moveImage(A, WINDOW_WIDTH/4 - imageArray[A].dest.w*1.5, WINDOW_HEIGHT*2/9 + imageArray[A].dest.h);
    moveImage(S, WINDOW_WIDTH /4 - imageArray[S].dest.w/2, WINDOW_HEIGHT*2/9 + imageArray[S].dest.h);
    moveImage(D, WINDOW_WIDTH /4 + imageArray[D].dest.w/2, WINDOW_HEIGHT*2/9 + imageArray[D].dest.h);
    moveImage(SPACE, WINDOW_WIDTH /4 - imageArray[SPACE].dest.w/2, WINDOW_HEIGHT*3/9 + imageArray[SPACE].dest.h);
    
    //p2
    int UP, LEFT, DOWN, RIGHT, ENTER;
    UP = loadImage("project-paradox/sprites/pressedUP.png");
    LEFT = loadImage("project-paradox/sprites/pressedLEFT.png");
    DOWN = loadImage("project-paradox/sprites/pressedDOWN.png");
    RIGHT = loadImage("project-paradox/sprites/pressedRIGHT.png");
    ENTER = loadImage("project-paradox/sprites/pressedRETURN.png");
    toggleActive(UP, 0);
    toggleActive(LEFT, 0);
    toggleActive(DOWN, 0);
    toggleActive(RIGHT, 0);
    toggleActive(ENTER, 0);
    moveImage(UP, WINDOW_WIDTH*3 / 4 - imageArray[UP].dest.w / 2, WINDOW_HEIGHT / 9 + imageArray[UP].dest.h);
    moveImage(LEFT, WINDOW_WIDTH*3 / 4 - imageArray[LEFT].dest.w * 1.5, WINDOW_HEIGHT * 2 / 9 + imageArray[LEFT].dest.h);
    moveImage(DOWN, WINDOW_WIDTH*3 / 4 - imageArray[DOWN].dest.w / 2, WINDOW_HEIGHT * 2 / 9 + imageArray[DOWN].dest.h);
    moveImage(RIGHT, WINDOW_WIDTH*3 / 4 + imageArray[RIGHT].dest.w / 2, WINDOW_HEIGHT * 2 / 9 + imageArray[RIGHT].dest.h);
    moveImage(ENTER, WINDOW_WIDTH*3/4 - imageArray[ENTER].dest.w/4, WINDOW_HEIGHT * 2 / 9 + imageArray[ENTER].dest.h);

    //buttons
    int exit = 0;
    int button1, button2;

    button1 = loadImage("project-paradox/sprites/button.png");
    moveImage(button1, 0, WINDOW_HEIGHT * 7 / 9);

    button2 = loadImage("project-paradox/sprites/button.png");
    moveImage(button2, WINDOW_WIDTH - imageArray[button2].dest.w, WINDOW_HEIGHT * 7 / 9);
    toggleFlip(button2, 1);


    //======players======
    player player1;
    createPlayer(&player1, 1);


    player player2;
    createPlayer(&player2, 2);
   
    player2.flip = 1;


   //cambio las posiciones de los jugadores
    player1.x_pos = WINDOW_WIDTH / 8;
    player1.y_pos = WINDOW_HEIGHT * 7 / 9;
    player2.x_pos = (WINDOW_WIDTH * 7 / 8) - player2.width;
    player2.y_pos = WINDOW_HEIGHT * 7 / 9;
    moveImage(player1.id, player1.x_pos, player1.y_pos);
    moveImage(player2.id, player2.x_pos, player2.y_pos);

  
    drawAllImages();
    updateWindow();
    clearWindow();
    

    while (!close_requested && exit ==0) {
        //listeners 

            eventListener(P1ptr, P2ptr);

            //========Player1 movement========
            playerMovement(P1ptr, &player1);

           
            //keys
            if (P1.up && !P1.down) {
         
                toggleActive(W, 1);
                toggleActive(S, 0);

            }else toggleActive(W, 0);


            if (P1.down && !P1.up) {

                toggleActive(S, 1);
                toggleActive(W, 0);

               
            }else toggleActive(S, 0);

            if (P1.left && !P1.right) {

                toggleActive(A, 1);
                toggleActive(D, 0);

            }else toggleActive(A, 0);

            if (P1.right && !P1.left) {

                toggleActive(D, 1);
                toggleActive(A, 0);

            }else toggleActive(D, 0);
           
            if (P1.action) toggleActive(SPACE, 1);
            else toggleActive(SPACE, 0);



          //========Player1 Collisions========
 

            //colission floor divisor
            if (player1.x_pos + player1.width >= imageArray[divisor].dest.x) {//COLISION PLAYER1 CON EL DIVISOR DESDE LA IZQUIERDA
                player1.x_pos = (WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2) - player1.width;
            }

            if (player1.y_pos + player1.height >= WINDOW_HEIGHT - imageArray[floor].dest.h) {//COLISION PLAYER1 CON EL SUELO
                player1.y_pos = (WINDOW_HEIGHT - imageArray[floor].dest.h) - player1.height;
            }

            //colission buttons
            if (player1.x_pos < imageArray[button1].dest.x + imageArray[button1].dest.w / 3 && player1.y_pos + player1.height >= imageArray[button1].dest.y + 6) {//COLISION PLAYER2 CON EL boton desde la derecha
                player1.x_pos = imageArray[button1].dest.x + imageArray[button1].dest.w /3;

            }
          
        
            moveImage(player1.id, player1.x_pos, player1.y_pos);
            toggleFlip(player1.id, player1.flip);

           
            

            //========Player2 Movement========
            playerMovement(P2ptr, &player2);

            //keys
            if (P2.up && !P2.down) {
               
                toggleActive(DOWN, 0);
                toggleActive(UP, 1);

            }else toggleActive(UP, 0);

            if (P2.down && !P2.up) {
               
                toggleActive(UP, 0);
                toggleActive(DOWN, 1);

               
            }else toggleActive(DOWN, 0);

            if (P2.left && !P2.right) {
               
                toggleActive(RIGHT, 0);
                toggleActive(LEFT, 1);

            }else toggleActive(LEFT, 0);

            if (P2.right && !P2.left) {
              
                
                toggleActive(LEFT, 0);
                toggleActive(RIGHT, 1);

            } else toggleActive(RIGHT, 0);

            if (P2.action) toggleActive(ENTER, 1);
            else toggleActive(ENTER, 0);
            
           
         

            //========Player2 Collisions========
           

            //colission floor divisor
            if (player2.x_pos <= imageArray[divisor].dest.x + imageArray[divisor].dest.w) {//COLISION PLAYER2 CON EL DIVISOR DESDE LA IZQUIERDA
                player2.x_pos = (WINDOW_WIDTH / 2 + imageArray[divisor].dest.w / 2);
            }

            if (player2.y_pos + player2.height >= WINDOW_HEIGHT - imageArray[floor].dest.h) {//COLISION PLAYER2 CON EL SUELO
                player2.y_pos = (WINDOW_HEIGHT - imageArray[floor].dest.h) - player2.height;
            }

            //colission buttons
         
            
           if (player2.x_pos + player2.width > imageArray[button2].dest.x + imageArray[button2].dest.w*2/3 && player2.y_pos + player2.height >= imageArray[button2].dest.y + 6) {//COLISION PLAYER2 CON EL boton desde la derecha
                player2.x_pos = imageArray[button2].dest.x + imageArray[button2].dest.w * 2 / 3  - player2.width;

           }
           
         
            moveImage(player2.id, player2.x_pos, player2.y_pos);
            toggleFlip(player2.id, player2.flip);

            //----botones action

            if (player1.x_pos < imageArray[button1].dest.x + imageArray[button1].dest.w && player1.y_pos + player1.height >= imageArray[button1].dest.y + 6 && P1.action || player2.x_pos + player2.width > imageArray[button2].dest.x  && player2.y_pos + player2.height > imageArray[button2].dest.y +6 && P2.action) {
                exit = 1;
                SDL_Delay(100);
            }

        

            SDL_Delay(1000 / FRAMES);

            drawAllImages();
            updateWindow();
            clearWindow();
        }



    return 0;
}
