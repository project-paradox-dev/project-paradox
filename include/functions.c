#include "../headers/functionHeader.h"
#include <SDL.h>
#include <SDL_timer.h>
#include <SDL_image.h>

#include "../headers/image.h"
#include "../headers/graphics.h"
//#include "level1.c"

//incluir time.h
//incluir random.h 

/*=========================================================================
MAIN MENU 
=========================================================================*/

int buttonPlay;
int buttonControls;
int currentPlayer = 0;
void loadMenu() {

 
    int offset = 40;

    int background;
    background = loadImage("project-paradox/menu/menuBackground.png");


    buttonPlay = loadImage("project-paradox/menu/menuPlay.png");
    int centerX = WINDOW_HEIGHT / 2 - offset - imageArray[buttonPlay].dest.h; // lo centramos aqui para coger la altura de un boton

    moveImage(buttonPlay, WINDOW_WIDTH/2 - imageArray[buttonPlay].dest.w/2, centerX);


    buttonControls = loadImage("project-paradox/menu/menuControls.png");
    offset = offset + imageArray[buttonPlay].dest.h;
    moveImage(buttonControls, WINDOW_WIDTH / 2 - imageArray[buttonControls].dest.w /2, centerX + offset );


}

int menuListener() {
    SDL_Event event;
    //mientras haya eventos, los ecuchamos 
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_QUIT:
            close_requested = 1;
            return 0;
            break;
        case SDL_MOUSEBUTTONDOWN: //cuando presiona el click
            printf("Mouse %d ha clickado en (%d,%d)\n",
                event.button.button, event.button.x, event.button.y);
            //if (event.button.x >= imageArray[buttonPlay].dest.x && event.button.x <= imageArray[buttonPlay].dest.x) {
            if (((event.button.x <= imageArray[buttonPlay].dest.x + imageArray[buttonPlay].dest.w) && (event.button.x >= imageArray[buttonPlay].dest.x)) &&
                ((event.button.y >= imageArray[buttonPlay].dest.y) && (event.button.y <= imageArray[buttonPlay].dest.y + imageArray[buttonPlay].dest.h)) && event.button.button == 1) {
                return 1;
                printf("SE HA HECHO CLICK DENTRO DE PLAY\n");
            }
            else if (((event.button.x <= imageArray[buttonControls].dest.x + imageArray[buttonControls].dest.w) && (event.button.x >= imageArray[buttonControls].dest.x)) &&
                ((event.button.y >= imageArray[buttonControls].dest.y) && (event.button.y <= imageArray[buttonControls].dest.y + imageArray[buttonControls].dest.h)) && event.button.button == 1) {
                return 2;
                printf("SE HA HECHO CLICK DENTRO DE controls\n");
            }
            break;
        }
        return 0;
    }
}
/*=========================================================================
EVENT LISTENER
=========================================================================*/

void eventListener( ctrls *P1ptr,  ctrls *P2ptr) {
    //procesar eventos, vemos los que hay
    SDL_Event event;
    //mientras haya eventos, los ecuchamos 
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_QUIT:
            close_requested = 1;
            break;
        case SDL_KEYDOWN: //cuando presiona la key
            switch (event.key.keysym.scancode) {
            case SDL_SCANCODE_W:
                //Decimos que en la posicion de memoria de P1ptr, 
                //cambiamos el valor up a 1, es decir, se mueve hacia arriba
                P1ptr->up = 1;
                break;
            case SDL_SCANCODE_UP:
                P2ptr->up = 1;
                break;
            case SDL_SCANCODE_A:
                P1ptr->left = 1;
                break;
            case SDL_SCANCODE_LEFT:
                P2ptr->left = 1;
                break;
            case SDL_SCANCODE_D:
                P1ptr->right = 1;
                break;
            case SDL_SCANCODE_RIGHT:
                P2ptr->right = 1;
                break;
            case SDL_SCANCODE_S:
                P1ptr->down = 1;
                break;
            case SDL_SCANCODE_DOWN:
                P2ptr->down = 1;
                break;
            case SDL_SCANCODE_SPACE:
                P1ptr->action =1;
                break;
            case SDL_SCANCODE_RETURN:
                P2ptr->action = 1;
                break;

            }
            break;

        case SDL_KEYUP: //cuando suelta la key
            switch (event.key.keysym.scancode) {
            case SDL_SCANCODE_W:
                P1ptr->up = 0;
                break;
            case SDL_SCANCODE_UP:
                P2ptr->up = 0;
                break;
            case SDL_SCANCODE_A:
                P1ptr->left = 0;
                break;
            case SDL_SCANCODE_LEFT:
                P2ptr->left = 0;
                break;
            case SDL_SCANCODE_D:
                P1ptr->right = 0;
                break;
            case SDL_SCANCODE_RIGHT:
                P2ptr->right = 0;
                break;
            case SDL_SCANCODE_S:
                P1ptr->down = 0;
                break;
            case SDL_SCANCODE_DOWN:
                P2ptr->down = 0;
                break;
            case SDL_SCANCODE_SPACE:
                P1ptr->action = 0;
                break;
            case SDL_SCANCODE_RETURN:
                P2ptr->action = 0;
                break;

            }
            break;
        }
    }
}

int loadLevel(int lvl) {
    int gameOutcome = 0;
    switch (lvl) {
        case 0:
            gameOutcome = startGamePlaceholder();
            break;
        case 1:
            gameOutcome = startGameFallingBlocks();
            break;
        case 2:
            gameOutcome = startGameCatchOrb();
            break;
        case 3:
            gameOutcome = startGameFreeFall();
            break;
        case 4:
            gameOutcome = startGameDuckDance();
            break;
        case 5:
            gameOutcome = startGameDragon();
            break;
   
    }
        
    return gameOutcome;
}

void countdown(int num) {
    //int countdownMax = 3, i;
    int c3, c2, c1;

    //suponiendo que todos los numeros tengan el mismo tamano, sacamos sus mitades para reusarlas
    
    int centerX = WINDOW_WIDTH / 2 - 40;
    int centerY = WINDOW_HEIGHT / 2;

        switch (num) {
            case 1:
                c3 = loadImage("project-paradox/sprites/3.png");
                moveImage(c3, centerX, centerY);
                break;
            case 2:
                c2 = loadImage("project-paradox/sprites/2.png");
                //toggleActive(c3, 0);
                moveImage(c2, centerX, centerY);
                break;

            case 3:
                c1 = loadImage("project-paradox/sprites/1.png");
                //toggleActive(c2, 0);
                moveImage(c1, centerX, centerY);
                break;
        }

   
}

int genRandomNumber(int min, int max) {
    int num;
    srand(time(NULL)); // Seed del numero actual
    num = rand() % (max - min + 1) + min;
    return num;
}

/*=========================================================================
PLAYER FUNCTIONS
=========================================================================*/

void createPlayer(player* playerPTR, int playerNumber) {
    if (playerNumber <= MAX_PLAYERS) {
        if (playerNumber == 1) {
            playerPTR->frame[0] = loadImage("project-paradox/sprites/player1_idle.png");
            playerPTR->frame[1] = loadImage("project-paradox/sprites/player1_move.png");
            playerPTR->frame[2] = loadImage("project-paradox/sprites/player1_jump.png");
            playerPTR->frame[3] = loadImage("project-paradox/sprites/player1_down.png");

        }
        else if (playerNumber == 2) {
            playerPTR->frame[0] = loadImage("project-paradox/sprites/player2_idle.png");
            playerPTR->frame[1] = loadImage("project-paradox/sprites/player2_move.png");
            playerPTR->frame[2] = loadImage("project-paradox/sprites/player2_jump.png");
            playerPTR->frame[3] = loadImage("project-paradox/sprites/player2_down.png");
        }

        playerPTR->id = playerPTR->frame[0];
        toggleActive(playerPTR->frame[1], 0);
        toggleActive(playerPTR->frame[2], 0);
        toggleActive(playerPTR->frame[3], 0);

        playerPTR->x_vel = 0;
        playerPTR->y_vel = 0;

        playerPTR->width = imageArray[playerPTR->id].dest.w;
        playerPTR->height = imageArray[playerPTR->id].dest.h;

        if (currentPlayer == 1) {
            playerPTR->x_pos = 0;
        }
        else if (currentPlayer == 2) {
            playerPTR->x_pos = WINDOW_WIDTH - playerPTR->width;
        }
        playerPTR->y_pos = 0;
        playerPTR->flip = 0;

        moveImage(playerPTR->id, playerPTR->x_pos, playerPTR->y_pos);
    }
    else {
        printf("ERROR: NO SE PERMITEN TANTOS JUGADORES\n");
    }
    
}

void playerMovement(ctrls* ctrlPTR, player* playerPTR) {
    playerPTR->x_vel = playerPTR->y_vel = 0;
    if (ctrlPTR->up && !ctrlPTR->down) {
        playerAnimation(playerPTR, 2, 2);
        playerPTR->y_vel = -SPEED;
    }
    if (ctrlPTR->down && !ctrlPTR->up) {
        playerAnimation(playerPTR, 2, 2);

        playerPTR->y_vel = SPEED;
    }
    if (ctrlPTR->left && !ctrlPTR->right) {
        playerPTR->flip = 1;
        playerAnimation(playerPTR, 2, 1);

        playerPTR->x_vel = -SPEED;
    }
    if (ctrlPTR->right && !ctrlPTR->left) {
        playerPTR->flip = 0;
        playerAnimation(playerPTR, 2, 1);

        playerPTR->x_vel = SPEED;
    }
    if (playerPTR->x_vel == 0 && playerPTR->y_vel == 0) {
        playerAnimation(playerPTR, 2, 0);

    }

    //cambio de pos si se sale de la pantalla
    playerPTR->x_pos += playerPTR->x_vel / FRAMES;
    playerPTR->y_pos += playerPTR->y_vel / FRAMES;
    if (playerPTR->x_pos + playerPTR->x_vel / FRAMES <= 0) {
        playerPTR->x_pos = 0;
    }
    if (playerPTR->y_pos + playerPTR->y_vel / FRAMES <= 0) {
        playerPTR->y_pos = 0;
    }
    if (playerPTR->x_pos + playerPTR->x_vel / FRAMES >= WINDOW_WIDTH - playerPTR->width) {
        playerPTR->x_pos = WINDOW_WIDTH - playerPTR->width;
    }
    if (playerPTR->y_pos + playerPTR->y_vel / FRAMES >= WINDOW_HEIGHT - playerPTR->height) {
        playerPTR->y_pos = WINDOW_HEIGHT - playerPTR->height;
    }
}

void playerAreaCollision(player* playerPTR, int divisorId, int floorId, int playerNumber) {
    
    
    if (playerPTR->x_pos + playerPTR->x_vel / FRAMES <= 0) {
        playerPTR->x_pos = 0;
    }
    if (playerPTR->y_pos + playerPTR->y_vel / FRAMES <= 0) {
        playerPTR->y_pos = 0;
    }
    if (playerPTR->x_pos + playerPTR->x_vel / FRAMES >= WINDOW_WIDTH - playerPTR->width) {
        playerPTR->x_pos = WINDOW_WIDTH - playerPTR->width;
    }
    if (playerPTR->y_pos + playerPTR->y_vel / FRAMES >= WINDOW_HEIGHT - playerPTR->height) {
        playerPTR->y_pos = WINDOW_HEIGHT - playerPTR->height;
    }

    if (playerNumber == 1) {
    //colission floor divisor
    if (playerPTR->x_pos + playerPTR->width >= imageArray[divisorId].dest.x) {//COLISION PLAYER1 CON EL DIVISOR DESDE LA IZQUIERDA
        playerPTR->x_pos = (WINDOW_WIDTH / 2 - imageArray[divisorId].dest.w / 2) - playerPTR->width;
    }

    if (playerPTR->y_pos + playerPTR->height >= WINDOW_HEIGHT - imageArray[floorId].dest.h) {//COLISION PLAYER1 CON EL SUELO
        playerPTR->y_pos = (WINDOW_HEIGHT - imageArray[floorId].dest.h) - playerPTR->height;
    }
    } else {
        if (playerPTR->x_pos + playerPTR->x_vel / FRAMES <= 0) {
            playerPTR->x_pos = 0;
        }
        if (playerPTR->y_pos + playerPTR->y_vel / FRAMES <= 0) {
            playerPTR->y_pos = 0;
        }
        if (playerPTR->x_pos + playerPTR->x_vel/ FRAMES >= WINDOW_WIDTH - playerPTR->width) {
            playerPTR->x_pos = WINDOW_WIDTH - playerPTR->width;
        }
        if (playerPTR->y_pos + playerPTR->y_vel / FRAMES >= WINDOW_HEIGHT - playerPTR->height) {
            playerPTR->y_pos = WINDOW_HEIGHT - playerPTR->height;
        }

        //colission floor divisor
        if (playerPTR->x_pos <= imageArray[divisorId].dest.x + imageArray[divisorId].dest.w) {//COLISION PLAYER1 CON EL DIVISOR DESDE LA IZQUIERDA
            playerPTR->x_pos = (WINDOW_WIDTH / 2 + imageArray[divisorId].dest.w / 2);
        }

        if (playerPTR->y_pos + playerPTR->height >= WINDOW_HEIGHT - imageArray[floorId].dest.h) {//COLISION PLAYER1 CON EL SUELO
            playerPTR->y_pos = (WINDOW_HEIGHT - imageArray[floorId].dest.h) - playerPTR->height;
        }
    }
}

void playerUpdate(player* playerPTR) {
    moveImage(playerPTR->id, playerPTR->x_pos, playerPTR->y_pos);
    toggleFlip(playerPTR->id, playerPTR->flip);
}

int playerObjectCollision(player* playerPTR, int objectID){
    int collision = 0;

    
    if ((playerPTR->x_pos <= imageArray[objectID].dest.x + imageArray[objectID].dest.w) &&
        (playerPTR->x_pos + playerPTR->width >= imageArray[objectID].dest.x) &&
        (playerPTR->y_pos + playerPTR->height >= imageArray[objectID].dest.y) &&
        (playerPTR->y_pos <= imageArray[objectID].dest.y + imageArray[objectID].dest.h)) {
        collision = 1;
    }//COLISION obs
    
    return collision;
}

void playerAnimation(player* sprite, int frameQty, int activeFrame) {
    int i;
    for (i = 0; i <= frameQty; i++) toggleActive(sprite->frame[i], 0);
    sprite->id = sprite->frame[activeFrame];
    toggleActive(sprite->frame[activeFrame], 1);
}
void drawWinner(int player) {
    int winner;

    deleteAllImages();


    if (player == 1) {
        winner = loadImage("project-paradox/sprites/win1.png");
    }
    else if (player == 2) {
        winner = loadImage("project-paradox/sprites/win2.png");
    }
    else if (player == 3) {
        winner = loadImage("project-paradox/sprites/win0.png");
    }
    drawAllImages();
    updateWindow();
    SDL_Delay(3000);
    clearWindow();
    deleteAllImages;
}
