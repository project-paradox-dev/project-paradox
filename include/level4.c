#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "../headers/functionHeader.h"


#include <SDL.h>
#include <SDL_timer.h>
#include <SDL_image.h>
#include "../headers/image.h"
#include "../headers/graphics.h"


int startGameDuckDance(void) {

    //creamos un puntero para el player 1 y su estructura de datos
    ctrls* P1ptr, P1;
    //inicializamos los valores de la estructura de datos 
    P1.up = 0;
    P1.down = 0;
    P1.left = 0;
    P1.right = 0;
    P1.action = 0;

    //asignamos los valores de la estructura de dato al puntero, y cuando 
    //queramos cambiar los valores de P1 desde fuera, usamos el puntero
    P1ptr = &P1;

    //lo mismo pero para P2 (player 2)
    ctrls* P2ptr, P2;

    P2.up = 0;
    P2.down = 0;
    P2.left = 0;
    P2.right = 0;
    P2.action = 0;

    P2ptr = &P2;

    //declaro los arrays de la secuencia a seguir y los input de los jugadores y las rondas que han pasado.
    int dance[4] = { 0, 0, 0, 0 }, p1_dance[4] = { 0, 0, 0, 0 }, p2_dance[4] = { 0, 0, 0, 0 };
    int i = 0, p1_i = 0, p2_i = 0, round = 0;

    int p1_points = 0, points_tmp, p2_points = 0, winner = 0;

    //variables para el tiempo
    time_t start_time, end_time;
   
    int random = (rand() % (10 - 20 + 1)) + 10;
    printf("%d", random);
    //========Floor & Divisor ========
    int bg;
    bg = loadImage("project-paradox/backgrounds/level4BG.png");

    int floor;
    floor = loadImage("project-paradox/sprites/floor.png");
    moveImage(floor, 0, WINDOW_HEIGHT - imageArray[floor].dest.h);
    drawImage(imageArray[floor].texture, &imageArray[floor].dest, 0);

    int divisor;
    divisor = loadImage("project-paradox/sprites/divisor.png");
    moveImage(divisor, WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2, 0);
    drawImage(imageArray[divisor].texture, &imageArray[divisor].dest, 0);

    //========Player1========
    player player1;
    createPlayer(&player1, 1);
    // moveImage(player1.id, player1.x_pos, player1.y_pos);
     //drawImage(imageArray[player1.id].texture, &imageArray[player1.id].dest, player1.flip);

    
    player player2;
    createPlayer(&player2, 2);
  
     //---------minigame---------



    //cambio las posiciones de los jugadores
    player1.x_pos = WINDOW_WIDTH / 8;
    player1.y_pos = WINDOW_HEIGHT * 7 / 9;
    player2.x_pos = (WINDOW_WIDTH * 7 / 8) - player2.width;
    player2.y_pos = WINDOW_HEIGHT * 7 / 9;
    moveImage(player1.id, player1.x_pos, player1.y_pos);
    moveImage(player2.id, player2.x_pos, player2.y_pos);

   



    //---Pato Dance---

    //declaro el pato de la izq

    player pato1; // se usa la estructura de jugadores para el pato porque tiene caracteristicas muy similares a las necesarias
    pato1.frame[0] = loadImage("project-paradox/sprites/pato_idle.png");
    pato1.frame[1] = loadImage("project-paradox/sprites/pato_up.png");
    pato1.frame[2] = loadImage("project-paradox/sprites/pato_down.png");
    pato1.frame[3] = loadImage("project-paradox/sprites/pato_side1.png");
    pato1.frame[4] = loadImage("project-paradox/sprites/pato_side1.png");
    pato1.id = pato1.frame[0];
    playerAnimation(&pato1, 4, 0);

    pato1.width = imageArray[pato1.id].dest.w;
    pato1.height = imageArray[pato1.id].dest.h;
    pato1.x_pos = WINDOW_WIDTH * 6 / 16;
    pato1.y_pos = WINDOW_HEIGHT / 4.5;
    pato1.flip = 1;
    toggleFlip(pato1.id, pato1.flip);
    moveImage(pato1.id, pato1.x_pos, pato1.y_pos);


    //declaro el pato de la der

    player pato2;
    pato2.frame[0] = loadImage("project-paradox/sprites/pato_idle.png");
    pato2.frame[1] = loadImage("project-paradox/sprites/pato_up.png");
    pato2.frame[2] = loadImage("project-paradox/sprites/pato_down.png");
    pato2.frame[3] = loadImage("project-paradox/sprites/pato_side1.png");
    pato2.frame[4] = loadImage("project-paradox/sprites/pato_side1.png");
    pato2.id = pato2.frame[0];
    playerAnimation(&pato2, 4, 0);

    pato2.width = imageArray[pato2.id].dest.w;
    pato2.height = imageArray[pato2.id].dest.h;
    pato2.x_pos = (WINDOW_WIDTH * 9 / 16);
    pato2.y_pos = WINDOW_HEIGHT / 4.5;
    moveImage(pato2.id, pato2.x_pos, pato2.y_pos);

    int ready, perfect1, perfect2;
   // ready = loadImage("project-paradox/sprites/readypocho.png");
    perfect1 = loadImage("project-paradox/sprites/perfect.png");
    perfect2 = loadImage("project-paradox/sprites/perfect.png");
    toggleActive(perfect1, 0);
    toggleActive(perfect2, 0);
    moveImage(perfect1, WINDOW_WIDTH / 8, WINDOW_HEIGHT * 4 / 8);
    moveImage(perfect2, (WINDOW_WIDTH * 5 / 8), WINDOW_HEIGHT * 4 / 8);


    drawAllImages();
    updateWindow();
    clearWindow();
    //toggleActive(ready, 0);

    while (!close_requested && round < 3) {
        //listeners 
         // ------------ MINIGAME ------------- 
        i = 0;
        SDL_Delay(100);
        //todos los frames a idle
        player1.id = player1.frame[0];
        playerAnimation(&player1, 3, 0);

        player2.id = player2.frame[0];
        playerAnimation(&player1, 3, 0);

        toggleActive(pato1.frame[0], 0);
        toggleActive(pato2.frame[0], 0);

        if (i == 0) {
            //randomizo numeros que representan la secuencia

            srand(time(NULL)); // Seed del numero actual
            for (i = 0; i < 4; i++) {
                dance[i] = rand() % 4;
                if (dance[i] == 0 || i > 0 && dance[i] == dance[i - 1]) {
                    i--;
                }
            }

        }
        for (i = 0; i < 4; i++) {

            //activo los frames correspondientes a cada movimiento de la secuencia
            pato1.id = pato1.frame[dance[i]];
            pato2.id = pato2.frame[dance[i]];
            moveImage(pato1.id, pato1.x_pos, pato1.y_pos);
            moveImage(pato2.id, pato2.x_pos, pato2.y_pos);


            SDL_Delay(200);
            toggleActive(pato1.frame[dance[i]], 1);
            toggleActive(pato2.frame[dance[i]], 1);
            if (dance[i] == 4) {
                pato1.flip = 0;
                pato2.flip = 1;
            }
            else if (dance[i] == 3) {
                pato1.flip = 1;
                pato2.flip = 0;
            }
            SDL_Delay(200);


            toggleFlip(pato1.id, pato1.flip);
            toggleFlip(pato2.id, pato2.flip);
            drawAllImages();
            updateWindow();
            clearWindow();
            toggleActive(pato1.frame[dance[i]], 0);
            toggleActive(pato2.frame[dance[i]], 0);

        }


        toggleActive(pato1.frame[0], 1);
        toggleActive(pato2.frame[0], 1);
        SDL_Delay(200);

        //
        start_time = SDL_GetTicks();
        end_time = SDL_GetTicks();


        p1_i = 0;
        p2_i = 0;
        //-------------------------------------
        while (end_time - start_time < 3000) {

            end_time = SDL_GetTicks();
            eventListener(P1ptr, P2ptr);

            //========Player1 Movement======== 
           
            
            //no se usa la funcion porque el movimiento no cambia de posición, solo la animación y la secuencia del array
            if (P1.up && !P1.down && p1_i < 4) {
  
                playerAnimation(&player1, 3, 2);

                p1_dance[p1_i] = 1;

                if (p1_dance[p1_i - 1] != p1_dance[p1_i] || p1_i == 0) p1_i++;
               

            }


            if (P1.down && !P1.up && p1_i < 4) {

                
                playerAnimation(&player1, 3, 3);
                
                p1_dance[p1_i] = 2;

                if (p1_dance[p1_i - 1] != p1_dance[p1_i] || p1_i == 0) p1_i++;
            }

            if (P1.left && !P1.right && p1_i < 4) {

                player1.flip = 1;
                
                playerAnimation(&player1, 3, 1);
                p1_dance[p1_i] = 3;

                if (p1_dance[p1_i - 1] != p1_dance[p1_i] || p1_i == 0) p1_i++;
               
            }

            if (P1.right && !P1.left && p1_i < 4) {

                player1.flip = 0;
                
                playerAnimation(&player1, 3, 1);

                p1_dance[p1_i] = 4;

                if (p1_dance[p1_i - 1] != p1_dance[p1_i] || p1_i == 0) p1_i++;
            }

            if (!P1.right && !P1.left && !P1.down && !P1.up) {

               
                playerAnimation(&player1, 3, 0);


            }


            moveImage(player1.id, player1.x_pos, player1.y_pos);
            toggleFlip(player1.id, player1.flip);


            //========Player2 Movement========
            player2.x_vel = player2.y_vel = 0;

            //no se usa la funcion porque el movimiento no cambia de posición, solo la animación y la secuencia del array

            if (P2.up && !P2.down && p2_i < 4) {
                
                playerAnimation(&player2, 3, 2);

                p2_dance[p2_i] = 1;

                if (p2_dance[p2_i - 1] != p2_dance[p2_i] || p2_i == 0) p2_i++;
               
            }
            if (P2.down && !P2.up && p2_i < 4) {
                player2.id = player2.frame[3];
                playerAnimation(&player2, 3, 3);

                
                p2_dance[p2_i] = 2;

                if (p2_dance[p2_i - 1] != p2_dance[p2_i] || p2_i == 0) p2_i++;

            }
            if (P2.left && !P2.right && p2_i < 4) {
                player2.flip = 1;
                player2.id = player2.frame[1];
                playerAnimation(&player2, 3, 1);

                p2_dance[p2_i] = 4;

                if (p2_dance[p2_i - 1] != p2_dance[p2_i] || p2_i == 0) p2_i++;

            }
            if (P2.right && !P2.left && p2_i < 4) {
                player2.flip = 0;
                player2.id = player2.frame[1];
                playerAnimation(&player2, 3, 1);

                
                p2_dance[p2_i] = 3;

                if (p2_dance[p2_i - 1] != p2_dance[p2_i] || p2_i == 0) p2_i++;

            }

            if (!P2.right && !P2.left && !P2.up && !P2.down) {
                player2.id = player2.frame[0];
                playerAnimation(&player2, 3, 0);


            }
            moveImage(player2.id, player2.x_pos, player2.y_pos);
            toggleFlip(player2.id, player2.flip);


            SDL_Delay(1000 / FRAMES);

            drawAllImages();
            updateWindow();
            clearWindow();
        }


       


        // comprueba la secuencia, si es igual suma un punto y aparece perfect
        if (cmpArrays(p1_dance, dance, 3) > 3) {
            toggleActive(perfect1, 1);
            p1_points++;
        }

        if (cmpArrays(p2_dance, dance, 3) > 3) {
            toggleActive(perfect2, 1);
            p2_points++;
        }


        
       

        
        SDL_Delay(50);
        drawAllImages();
        updateWindow();
        clearWindow();
        toggleActive(perfect1, 0);
        toggleActive(perfect2, 0);

        for (i = 0; i < 4; i++)
        {
            printf("\n%d - %d = %d", p1_dance[i], p2_dance[i], dance[i]);
            p1_dance[i] = 0;
            p2_dance[i] = 0;
        }

        round++;   

    }

    SDL_Delay(100);

    if (p1_points > p2_points) winner = 1;
    else if (p2_points > p1_points) winner = 2;

    return winner;
}

int cmpArrays(int v1[], int v2[], int size) {
    int i = 0;

    while (i <= size && v1[i] == v2[i]) {
        i++;
    }

    return i;
}

