#include <stdio.h>
#include <string.h>

#include <SDL.h>
#include <SDL_timer.h>
#include <SDL_image.h>

#include "../headers/image.h"
#include "../headers/graphics.h"
#include "../headers/functionHeader.h"






//MANTENIMIENTO DE CODIGO
//ESTA MAL COMENTADO
//FALTA CAMBIO DE SPRITE, HE CAMBIADO MAXIMOS

int RNG(int max) {
    int r = rand() % max;

    return r;
}
int startGameCatchOrb() {
    //creamos un puntero para el player 1 y su estructura de datos
    ctrls* P1ptr, P1;

    //inicializamos los valores de la estructura de datos 
    P1.up = 0;
    P1.down = 0;
    P1.left = 0;
    P1.right = 0;
    P1.action = 0;

    //asignamos los valores de la estructura de dato al puntero, y cuando 
    //queramos cambiar los valores de P1 desde fuera, usamos el puntero
    P1ptr = &P1;

    //lo mismo pero para P2 (player 2)
    ctrls* P2ptr, P2;

    P2.up = 0;
    P2.down = 0;
    P2.left = 0;
    P2.right = 0;
    P2.action = 0;

    P2ptr = &P2;


  
    //======== Background ========
    int background;
    background = loadImage("project-paradox/backgrounds/level2BG.png");

    //========Floor & Divisor ========

    int floor;
    floor = loadImage("project-paradox/sprites/floor.png");
    moveImage(floor, 0, WINDOW_HEIGHT - imageArray[floor].dest.h);
    drawImage(imageArray[floor].texture, &imageArray[floor].dest, 0);

    int divisor;
    divisor = loadImage("project-paradox/sprites/divisor.png");
    moveImage(divisor, WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2, 0);
    drawImage(imageArray[divisor].texture, &imageArray[divisor].dest, 0);


    //========Player1========
    player* player1PTR, player1;
    player1PTR = &player1;
    createPlayer(player1PTR, 1);

    //========Player2========
    player* player2PTR, player2;
    player2PTR = &player2;
    createPlayer(player2PTR, 2);
    player2.x_pos = WINDOW_WIDTH - player2.width;
    moveImage(player2.id, player2.x_pos, 0);
    drawImage(imageArray[player1.id].texture, &imageArray[player1.id].dest, player1.flip);

    drawImage(imageArray[player2.id].texture, &imageArray[player2.id].dest, player2.flip);


    /*
    int test;
    test = loadImage("project-paradox/sprites/player1.png");
    moveImage(test, 0 , 0);
    drawImage(imageArray[test].texture, &imageArray[test].dest);
    */
    //LO MIO
    //random
    srand(time(NULL));
    int windowRightMax;
    int points2 = 0, points1 = 0;
    //========Cube and boost===========
    int cube;
    int pos_x_cube2 = 800;
    int pos_y_cube2 = 200;
    int pos_x_cube = 200;
    int pos_y_cube = 200;
    int endTimer = 7000;
    //boost1
    int boost1 = 0;
    int boostY1 = 0;
    float boostTimer1 = 0;
    float boostCD1 = 0;
    //boost2
    int boost2 = 0;
    int boostY2 = 0;
    float boostTimer2 = 0;
    float boostCD2 = 0;
    cube = loadImage("project-paradox/sprites/orb.png");
    moveImage(cube, pos_x_cube, pos_y_cube);
    drawImage(imageArray[cube].texture, &imageArray[cube].dest, 0);
    int cube2;
    cube2 = loadImage("project-paradox/sprites/orb.png");
    moveImage(cube2, pos_x_cube2, pos_y_cube2);
    drawImage(imageArray[cube2].texture, &imageArray[cube2].dest, 0);

    
    //Gestion de imagenes de boost, Active es para cuando el boost esta activo
    int boostImageActive1 = loadImage("project-paradox/sprites/boostActive.png");
    moveImage(boostImageActive1,10, imageArray[floor].dest.y );
    int boostImageInactive1 = loadImage("project-paradox/sprites/boostNotActive.png");
    moveImage(boostImageInactive1, 10, imageArray[floor].dest.y);
    toggleActive(boostImageInactive1, 0);
    drawImage(imageArray[boostImageInactive1].texture, &imageArray[boostImageInactive1].dest, 0);
    int boostImageActive2 = loadImage("project-paradox/sprites/boostActive.png");
    moveImage(boostImageActive2,WINDOW_WIDTH-imageArray[boostImageActive2].dest.w-10, imageArray[floor].dest.y);
    int boostImageInactive2 = loadImage("project-paradox/sprites/boostNotActive.png");
    moveImage(boostImageInactive2, WINDOW_WIDTH - imageArray[boostImageActive2].dest.w - 10, imageArray[floor].dest.y);
    toggleActive(boostImageInactive2, 0);
    drawImage(imageArray[boostImageInactive2].texture, &imageArray[boostImageInactive2].dest, 0);
    int maxPoints = 7;
    int exitCatchOrbs = 0;

    while ((!close_requested)&&(exitCatchOrbs==0)) {
        //listeners 
        eventListener(P1ptr, P2ptr);


        //Boost
        if (P1.action) {


            if (boostY1 == 0) {
                boost1 = 400;
                boostTimer1 = 250;
                toggleActive(boostImageActive1, 0);
                toggleActive(boostImageInactive1 , 1);
                boostY1 = 1;
            }

        }
        if (P2.action) {


            if (boostY2 == 0) {
                boost2 = 400;
                boostTimer2 = 250;
                toggleActive(boostImageActive2, 0);
                toggleActive(boostImageInactive2, 1);
                boostY2 = 1;
            }

        }
        //========Player1 Movement========
        player1.x_vel = player1.y_vel = 0;
        if (P1.up && !P1.down) {
            player1.id = player1.frame[2];
            toggleActive(player1.frame[1], 0);
            toggleActive(player1.frame[0], 0);
            toggleActive(player1.frame[2], 1);
            player1.y_vel = -SPEED - boost1;
        }
        if (P1.down && !P1.up) {
            player1.id = player1.frame[2];
            toggleActive(player1.frame[1], 0);
            toggleActive(player1.frame[0], 0);
            toggleActive(player1.frame[2], 1);
            player1.y_vel = SPEED + boost1;

        }
        if (P1.left && !P1.right) {
            player1.flip = 1;
            player1.id = player1.frame[1];
            toggleActive(player1.frame[2], 0);
            toggleActive(player1.frame[0], 0);
            toggleActive(player1.frame[1], 1);

            player1.x_vel = -SPEED - boost1;
        }
        if (P1.right && !P1.left) {
            player1.flip = 0;
            player1.id = player1.frame[1];
            toggleActive(player1.frame[2], 0);
            toggleActive(player1.frame[0], 0);
            toggleActive(player1.frame[1], 1);
            player1.x_vel = SPEED + boost1;
        }
        if (player1.x_vel == 0 && player1.y_vel == 0) {
            player1.id = player1.frame[0];
            toggleActive(player1.frame[2], 0);
            toggleActive(player1.frame[1], 0);
            toggleActive(player1.frame[0], 1);

        }

        //cambio de pos
        player1.x_pos += player1.x_vel / FRAMES;
        player1.y_pos += player1.y_vel / FRAMES;


        //========Player1 Collisions========
        playerAreaCollision(player1PTR, divisor, floor, 1);
        playerUpdate(player1PTR);

        //collision cube
        if (playerObjectCollision(player1PTR, cube) == 1) {//COLISION obstaculo CON EL jugador DESDE los lados

            pos_x_cube2 = RNG(WINDOW_WIDTH / 2 - imageArray[divisor].dest.w - imageArray[cube2].dest.w);

            while ((pos_x_cube - player1.x_pos < 200 && pos_x_cube - player1.x_pos >-200) || (pos_y_cube - player1.y_pos < 200 && pos_y_cube - player1.y_pos >-200)) {
                pos_x_cube = RNG(WINDOW_WIDTH / 2 - imageArray[divisor].dest.w - imageArray[cube].dest.w);
                pos_y_cube = RNG(WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[cube].dest.h);
            }
            pos_y_cube = RNG(WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[cube].dest.h);

            moveImage(cube, pos_x_cube, pos_y_cube);
            points1++;
           
        }

        //colisionescubo2
        // con esto detectamos cuando se mete el player2 en el cubo de puntos y hacemos el algoritmo de spawn aleatorio
        if (playerObjectCollision(player2PTR,cube2)==1) {//COLISION obstaculo CON EL jugador DESDE los lados

            windowRightMax = (WINDOW_WIDTH / 2 + imageArray[divisor].dest.w / 2);
            pos_x_cube2 = RNG(WINDOW_WIDTH / 2 - imageArray[divisor].dest.w - imageArray[cube2].dest.w);
            pos_x_cube2 = pos_x_cube2 + windowRightMax;
            while ((pos_x_cube2 - player2.x_pos < 200 && pos_x_cube2 - player2.x_pos >-200) || (pos_y_cube2 - player2.y_pos < 200 && pos_y_cube2 - player2.y_pos >-200)) {
                pos_x_cube2 = RNG(WINDOW_WIDTH / 2 - imageArray[divisor].dest.w - imageArray[cube2].dest.w);
                pos_x_cube2 = pos_x_cube2 + windowRightMax;
                pos_y_cube2 = RNG(WINDOW_HEIGHT - imageArray[floor].dest.h - imageArray[cube2].dest.h);
            }

            moveImage(cube2, pos_x_cube2, pos_y_cube2);
            points2++;
            
        }
        //========Player2 Movement========
        player2.x_vel = player2.y_vel = 0;
        if (P2.up && !P2.down) {
            player2.id = player2.frame[2];
            toggleActive(player2.frame[1], 0);
            toggleActive(player2.frame[0], 0);
            toggleActive(player2.frame[2], 1);
            player2.y_vel = -SPEED - boost2;
        }
        if (P2.down && !P2.up) {
            player2.id = player2.frame[2];
            toggleActive(player2.frame[1], 0);
            toggleActive(player2.frame[0], 0);
            toggleActive(player2.frame[2], 1);
            player2.y_vel = SPEED + boost2;

        }
        if (P2.left && !P2.right) {
            player2.flip = 1;
            player2.id = player2.frame[1];
            toggleActive(player2.frame[2], 0);
            toggleActive(player2.frame[0], 0);
            toggleActive(player2.frame[1], 1);

            player2.x_vel = -SPEED - boost2;
        }
        if (P2.right && !P2.left) {
            player2.flip = 0;
            player2.id = player2.frame[1];
            toggleActive(player2.frame[2], 0);
            toggleActive(player2.frame[0], 0);
            toggleActive(player2.frame[1], 1);
            player2.x_vel = SPEED + boost2;

        }
        if (player2.x_vel == 0 && player2.y_vel == 0) {
            player2.id = player2.frame[0];
            toggleActive(player2.frame[2], 0);
            toggleActive(player2.frame[1], 0);
            toggleActive(player2.frame[0], 1);

        }

        //cambio de pos
        player2.x_pos += player2.x_vel / FRAMES;
        player2.y_pos += player2.y_vel / FRAMES;

        //========Player2 Collisions========
        playerAreaCollision(player2PTR, divisor, floor, 2);
        playerUpdate(player2PTR);


        // Deteccion de victoria
        if (points1 >= maxPoints) { //deteccion de victoria jugador 1
            exitCatchOrbs = 2;

        }
        if (points2 >= maxPoints) {//deteccion de victoria jugador 2
            exitCatchOrbs = 3;

        }
        if (points1 == maxPoints && points2 == maxPoints) {//deteccion de empate
            exitCatchOrbs = 1;
        }


        drawAllImages();
        updateWindow();
        clearWindow();






        //para hacer el cd de el boost hacemos una resta cada vez que el boostTimer sea mayor que cero asi seria como un contador

        if ((boostTimer1 != 0) && (boostTimer1 > 0)) {
            boostTimer1 = boostTimer1 - (1000 / FRAMES);
        }
        if (boostTimer1 <= 0) {
            boost1 = 0;//cuando boost se ponga a 0 el boost termina.
        }
        if (boostY1 == 1) {

            boostCD1 = boostCD1 + (1000 / FRAMES);
            if (boostCD1 >= 500) {
                boostY1 = 0;
                boostCD1 = 0;
                toggleActive(boostImageActive1, 1);
                toggleActive(boostImageInactive1, 0);
            }
        }
        if (endTimer > 0) {
            endTimer -= 1000 / FRAMES;
        }
        if (endTimer <= 0) {
            exitCatchOrbs = 4;
        }
        if ((boostTimer2 != 0) && (boostTimer2 > 0)) {
            boostTimer2 = boostTimer2 - (1000 / FRAMES);
        }
        if (boostTimer2 <= 0) {
            boost2 = 0;
        }
        if (boostY2 == 1) {

            boostCD2 = boostCD2 + (1000 / FRAMES);
            if (boostCD2 >= 500) {
                boostY2 = 0;
                boostCD2 = 0;
                toggleActive(boostImageActive2, 1);
                toggleActive(boostImageInactive2, 0);

            }
        }
        SDL_Delay(1000 / FRAMES);
    }
    switch (exitCatchOrbs){
    case 1:
        return 0;
        break;
    case 2:
        return 1;
        break;
    case 3:
        return 2;
        break;
    case 4:
        if (points1 > points2) {
            return 1;
        }
        else if (points1 < points2) {
            return 2;
        }
        else if (points1 == points2) {
            return 0;
        }
        break;
}
}


