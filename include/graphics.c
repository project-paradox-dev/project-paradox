#include <stdlib.h>
#include <stdio.h>

#define GRAPHICS
//#include "SDL_ttf.h"

#include "../headers/graphics.h"
#include "../headers/image.h"

SDL_Window* window = NULL;
SDL_Renderer* gRenderer;

SDL_Renderer* getRenderer(void) { return gRenderer; }



int initSDL() {
    //se ha podido inicializar el SDL? si no, salimos del programa
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0)
    {
        printf("Ezin da SDL hasieratu, errorea: %s\n", SDL_GetError());
        return -1;
    }
    atexit(SDL_Quit);
    //titulo,posicionx,posiciony,res,res,flags
    SDL_Window* window = SDL_CreateWindow("Project Paradox", SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        WINDOW_WIDTH, WINDOW_HEIGHT, 0);
    if (!window) {
        printf("Ezin da leihoa pantailaratu: %s\n", SDL_GetError());
        SDL_Quit();
        return -1;
    }


    Uint32 render_flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
    //-1 indica que no usamos ningun driver grafico especifico
    gRenderer = SDL_CreateRenderer(window, -1, render_flags);

    if (!gRenderer) {
        printf("Ezin da renderer hasieratu: %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return -1;
    }

    printf("SDL hasieratu da!\n");
    return 0;
}

void endSDL(){
    //finalizamos el programa
    SDL_DestroyWindow(window);
    SDL_Quit();
}

int drawImage(SDL_Texture* texture, SDL_Rect* pDest, int flip)
{
    SDL_Rect src;

    src.x = 0;
    src.y = 0;
    src.w = pDest->w;
    src.h = pDest->h;
    if (flip) SDL_RenderCopyEx(gRenderer, texture, &src, pDest, 0, NULL, SDL_FLIP_HORIZONTAL);
    else SDL_RenderCopy(gRenderer, texture, &src, pDest);

    //SDL_RenderPresent(gRenderer);
    return 0;
}

void clearWindow() {
    SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(gRenderer);
}

void updateWindow() {
    SDL_RenderPresent(gRenderer);
}