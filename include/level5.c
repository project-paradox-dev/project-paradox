#include <stdio.h>
#include <string.h>

#include <SDL.h>
#include <SDL_timer.h>
#include <SDL_image.h>

#include "../headers/image.h"
#include "../headers/graphics.h"
#include "../headers/functionHeader.h"


int startGameDragon() {

    int gameOutcome = 0, i;
    int platformOffset = 130;


    //creamos un puntero para el player 1 y su estructura de datos
    ctrls* P1ptr, P1;
    //inicializamos los valores de la estructura de datos 
    P1.up = 0;
    P1.down = 0;
    P1.left = 0;
    P1.right = 0;
    P1.action = 0;

    //asignamos los valores de la estructura de dato al puntero, y cuando 
    //queramos cambiar los valores de P1 desde fuera, usamos el puntero
    P1ptr = &P1;

    //lo mismo pero para P2 (player 2)
    ctrls* P2ptr, P2;

    P2.up = 0;
    P2.down = 0;
    P2.left = 0;
    P2.right = 0;
    P2.action = 0;

    P2ptr = &P2;
    //SDL_Delay(1000);
    //========Floor , Divisor , Background & Exit========
    int background;
    background = loadImage("project-paradox/backgrounds/level5BG.png");

    int floor;
    floor = loadImage("project-paradox/sprites/floor.png");
    moveImage(floor, 0, WINDOW_HEIGHT - imageArray[floor].dest.h);
    drawImage(imageArray[floor].texture, &imageArray[floor].dest, 0);

    int divisor;
    divisor = loadImage("project-paradox/sprites/divisor.png");
    moveImage(divisor, WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2, 0);
    drawImage(imageArray[divisor].texture, &imageArray[divisor].dest, 0);

    int exitL = loadImage("project-paradox/sprites/exit.png");
    moveImage(imageArray[exitL].id, 0, platformOffset - imageArray[exitL].dest.h);

    int exitR = loadImage("project-paradox/sprites/exit.png");
    moveImage(imageArray[exitR].id, WINDOW_WIDTH - imageArray[exitR].dest.w, platformOffset - imageArray[exitL].dest.h);

    int treasureP1;
    treasureP1 = loadImage("project-paradox/sprites/treasure.png");
    moveImage(imageArray[treasureP1].id, 480, WINDOW_HEIGHT - imageArray[floor].dest.h*2 );

    int treasureP2;
    treasureP2 = loadImage("project-paradox/sprites/treasure.png");
    moveImage(imageArray[treasureP2].id, WINDOW_WIDTH - 480 - imageArray[treasureP2].dest.w, WINDOW_HEIGHT - imageArray[floor].dest.h * 2);

    //========Player1========
    player* player1PTR, player1;
    player1PTR = &player1;
    createPlayer(player1PTR, 1);
    int player1HasTreasure = 0;
    //========Player2========
    player* player2PTR, player2;
    player2PTR = &player2;
    createPlayer(player2PTR, 2);
    player2.x_pos = WINDOW_WIDTH - player2.width;
    moveImage(player2.id, player2.x_pos, 0);
    int player2HasTreasure = 0;

    //========Carga de ojos========
    int eye1Closed = loadImage("project-paradox/sprites/eye1.png");
    moveImage(eye1Closed, 200, WINDOW_HEIGHT - imageArray[floor].dest.h*2 - 35);
    int eye1Half = loadImage("project-paradox/sprites/eye2.png");
    moveImage(eye1Half, 200, WINDOW_HEIGHT - imageArray[floor].dest.h*2 - 35);
    int eye1Open = loadImage("project-paradox/sprites/eye3.png");
    moveImage(eye1Open, 200, WINDOW_HEIGHT - imageArray[floor].dest.h*2 - 35);

    int eye2Closed = loadImage("project-paradox/sprites/eye1.png");
    moveImage(eye2Closed, WINDOW_WIDTH -200 - imageArray[eye2Closed].dest.w, WINDOW_HEIGHT - imageArray[floor].dest.h * 2 - 35);
    int eye2Half = loadImage("project-paradox/sprites/eye2.png");
    moveImage(eye2Half, WINDOW_WIDTH - 200 - imageArray[eye2Closed].dest.w, WINDOW_HEIGHT - imageArray[floor].dest.h * 2 - 35);
    int eye2Open = loadImage("project-paradox/sprites/eye3.png");
    moveImage(eye2Open, WINDOW_WIDTH - 200 - imageArray[eye2Closed].dest.w, WINDOW_HEIGHT - imageArray[floor].dest.h * 2 - 35);

    //========Plataformas y Exit========


    //generar plataformas de la izquierda
    Objects platformsL[3];

    for (i = 0; i < 3; i++) {
        //asignacion de valores a la estructura
        platformsL[i].id = loadImage("project-paradox/sprites/woodPlatform.png");
        platformsL[i].width = imageArray[platformsL[i].id].dest.w;
        platformsL[i].height = imageArray[platformsL[i].id].dest.h;
        //esto alterna donde empieza la plataforma, esto nos permite anadir mas plataformas en un futuro
        //y dejando un camino libre hacia abajo siempre
        //es modular!!
        if (i == 0) {
            platformsL[i].x_pos = 0;
            platformsL[i].y_pos = platformOffset;
        } else if (i % 2 == 0) {
            platformsL[i].x_pos = 0;
            platformsL[i].y_pos = platformsL[i - 1].y_pos + platformOffset;
            
        } else {
            platformsL[i].x_pos = WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2 - platformsL[i].width;
            platformsL[i].y_pos = platformsL[i - 1].y_pos + platformOffset;
        }
       
        moveImage(platformsL[i].id, platformsL[i].x_pos, platformsL[i].y_pos);
    }
    //generamos plataforas de la derecha
    Objects platformsR[3];

    for (i = 0; i < 3; i++) {
        platformsR[i].id = loadImage("project-paradox/sprites/woodPlatform.png");
        platformsR[i].width = imageArray[platformsR[i].id].dest.w;
        platformsR[i].height = imageArray[platformsR[i].id].dest.h;
        if (i == 0) {
            platformsR[i].x_pos = WINDOW_WIDTH - platformsR[i].width;
            platformsR[i].y_pos = platformOffset;
        }
        else if (i % 2 == 0) {
            platformsR[i].x_pos = WINDOW_WIDTH - platformsR[i].width;
            platformsR[i].y_pos = platformsR[i - 1].y_pos + platformOffset;

        }
        else {
            platformsR[i].x_pos = WINDOW_WIDTH / 2 + imageArray[divisor].dest.w / 2;
            platformsR[i].y_pos = platformsR[i - 1].y_pos + platformOffset;
        }
        moveImage(platformsR[i].id, platformsR[i].x_pos, platformsR[i].y_pos);
    }

    toggleActive(eye1Half, 0);
    toggleActive(eye1Open, 0);
    toggleActive(eye2Half, 0);
    toggleActive(eye2Open, 0);

    int gameTime = 2300;
    drawAllImages;
    int timer = 500;
    int eyeState = 0;
    /*
    int test;
    test = loadImage("project-paradox/sprites/player1.png");
    moveImage(test, 0 , 0);
    drawImage(imageArray[test].texture, &imageArray[test].dest);
    */
    while (!close_requested && gameOutcome ==0 && gameTime !=0) {
        //listeners 
        gameTime--;
        //printf("%d\n", gameTime);
        eventListener(P1ptr, P2ptr);

        //cambio sprite ojo
        //pasar a un switch cuando se pueda
        switch (timer) {
            case 300:
                toggleActive(eye1Closed, 1);
                toggleActive(eye1Half, 0);
                toggleActive(eye1Open, 0);

                toggleActive(eye2Closed, 1);
                toggleActive(eye2Half, 0);
                toggleActive(eye2Open, 0);
                eyeState = 1;
                break;
            case 200:
                toggleActive(eye1Closed, 0);
                toggleActive(eye1Half, 1);
                toggleActive(eye1Open, 0);
                toggleActive(eye2Closed, 0);
                toggleActive(eye2Half, 1);
                toggleActive(eye2Open, 0);
                eyeState = 2;
                break;
            case 100:
                toggleActive(eye1Closed, 0);
                toggleActive(eye1Half, 0);
                toggleActive(eye1Open, 1);
                toggleActive(eye2Closed, 0);
                toggleActive(eye2Half, 0);
                toggleActive(eye2Open, 1);
                eyeState = 3;
                break;
            case 0:
                toggleActive(eye1Closed, 0);
                toggleActive(eye1Half, 1);
                toggleActive(eye1Open, 0);

                toggleActive(eye2Closed, 0);
                toggleActive(eye2Half, 1);
                toggleActive(eye2Open, 0);
                eyeState = 2;
                timer = 301;
                break;

        }     
        timer--;

        

        //========Player1 Movement========
        playerMovement(P1ptr, player1PTR);
        //========Player1 Collisions========
        playerAreaCollision(player1PTR, divisor, floor, 1);
        playerUpdate(player1PTR);

        //========Player2 Movement========
        playerMovement(P2ptr, player2PTR);
        //========Player2 Collisions========
        playerAreaCollision(player2PTR, divisor, floor, 2);
        playerUpdate(player2PTR);

        //TODO: Pasar a funcion
        //player1 colisiones plataforma
        if (player1.y_pos + player1.height <= platformsL[0].y_pos + platformsL[0].height && playerObjectCollision(player1PTR, platformsL[0].id)) {
            player1.y_pos = platformsL[0].y_pos - player1.height;

        }
        else if (player1.y_pos + player1.height <= platformsL[1].y_pos + platformsL[1].height && playerObjectCollision(player1PTR, platformsL[1].id)) {
            player1.y_pos = platformsL[1].y_pos - player1.height;

        } else if (player1.y_pos + player1.height <= platformsL[2].y_pos + platformsL[2].height && playerObjectCollision(player1PTR, platformsL[2].id)) {
            player1.y_pos = platformsL[2].y_pos - player1.height;

        }
        if (player1.y_pos + player1.height >= platformsL[0].y_pos + platformsL[0].height/2 && playerObjectCollision(player1PTR, platformsL[0].id)) {
            player1.y_pos = platformsL[0].y_pos + platformsL[0].height;
        } else if (player1.y_pos + player1.height >= platformsL[1].y_pos + platformsL[1].height / 2 && playerObjectCollision(player1PTR, platformsL[1].id)) {
                player1.y_pos = platformsL[1].y_pos + platformsL[1].height;
        }
        else if (player1.y_pos + player1.height >= platformsL[1].y_pos + platformsL[1].height / 2 && playerObjectCollision(player1PTR, platformsL[1].id)) {
            player1.y_pos = platformsL[1].y_pos + platformsL[1].height;
        }


        //player2 colisiones plataforma
        if (player2.y_pos + player2.height <= platformsR[0].y_pos + platformsR[0].height && playerObjectCollision(player2PTR, platformsR[0].id)) {
            player2.y_pos = platformsR[0].y_pos - player2.height;

        }
        else if (player2.y_pos + player2.height <= platformsR[1].y_pos + platformsR[1].height && playerObjectCollision(player2PTR, platformsR[1].id)) {
            player2.y_pos = platformsR[1].y_pos - player2.height;

        }
        else if (player2.y_pos + player2.height <= platformsR[2].y_pos + platformsR[2].height && playerObjectCollision(player2PTR, platformsR[2].id)) {
            player2.y_pos = platformsR[2].y_pos - player2.height;

        }
        if (player2.y_pos + player2.height >= platformsR[0].y_pos + platformsR[0].height / 2 && playerObjectCollision(player2PTR, platformsR[0].id)) {
            player2.y_pos = platformsR[0].y_pos + platformsR[0].height;
        }
        else if (player2.y_pos + player2.height >= platformsR[1].y_pos + platformsR[1].height / 2 && playerObjectCollision(player2PTR, platformsR[1].id)) {
            player2.y_pos = platformsR[1].y_pos + platformsR[1].height;
        }
        else if (player2.y_pos + player2.height >= platformsR[1].y_pos + platformsR[1].height / 2 && playerObjectCollision(player2PTR, platformsR[1].id)) {
            player2.y_pos = platformsR[1].y_pos + platformsR[1].height;
        }

        // GESTION DE OJO

        if (eyeState == 3 && (player1.x_vel != 0 || player1.y_vel != 0)) {
            player1.x_pos = player1.y_pos = 0;
            moveImage(player1.id, player1.x_pos, player1.y_pos);

            player1HasTreasure = 0;
            toggleActive(treasureP1, 1);
        }
        if (eyeState == 3 && (player2.x_vel != 0 || player2.y_vel != 0)) {
            player2.x_pos = WINDOW_WIDTH - player2.width ;
            player2.y_pos = 0;
            moveImage(player2.id, player2.x_pos, player2.y_pos);

            player2HasTreasure = 0;
            toggleActive(treasureP2, 1);
        }

        if (playerObjectCollision(player1PTR, treasureP1) && player1HasTreasure == 0) {
            player1HasTreasure = 1;
            toggleActive(treasureP1, 0);
        }
        if (playerObjectCollision(player2PTR, treasureP2) && player2HasTreasure == 0) {
            player2HasTreasure = 1;
            toggleActive(treasureP2, 0);
        }

        if (playerObjectCollision(player1PTR, exitL) && player1HasTreasure == 1) {
            gameOutcome = 1;
        }
        if (playerObjectCollision(player2PTR, exitR) && player2HasTreasure == 1) {
            gameOutcome = 2;
        }

        drawAllImages();
        updateWindow();
        clearWindow();


        //dibujamos la imagen en la ventana
        //con que queremos renderizarlo, la textura, posicion en el spritesheet y destino
        // SDL_RenderCopy(gRenderer, texture, NULL, &destination);
        //SDL_RenderPresent(gRenderer);



        //esperamos para conseguir el framerate que queremos
        SDL_Delay(1000 / FRAMES);
    }
    if (gameTime == 0) {
        gameOutcome = 0;
    }
    return gameOutcome;
}