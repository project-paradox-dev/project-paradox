#include <stdio.h>
#include <string.h>

#include <SDL.h>
#include <SDL_timer.h>
#include <SDL_image.h>

#include "../headers/image.h"
#include "../headers/graphics.h"
#include "../headers/functionHeader.h"


int startGamePlaceholder(){

    int gameOutcome;
   //creamos un puntero para el player 1 y su estructura de datos
    ctrls* P1ptr, P1;
    //inicializamos los valores de la estructura de datos 
    P1.up = 0;
    P1.down = 0;
    P1.left = 0;
    P1.right = 0;
    P1.action = 0;

    //asignamos los valores de la estructura de dato al puntero, y cuando 
    //queramos cambiar los valores de P1 desde fuera, usamos el puntero
    P1ptr = &P1;

    //lo mismo pero para P2 (player 2)
    ctrls* P2ptr, P2;

    P2.up = 0;
    P2.down = 0;
    P2.left = 0;
    P2.right = 0;
    P2.action = 0;

    P2ptr = &P2;
    //SDL_Delay(1000);
        //========Floor & Divisor ========
   // int test;
   // test = loadImage("project-paradox/backgrounds/level2BG.png");

    int floor;
    floor = loadImage("project-paradox/sprites/floor.png");
    moveImage(floor, 0, WINDOW_HEIGHT - imageArray[floor].dest.h);
    drawImage(imageArray[floor].texture, &imageArray[floor].dest, 0);

    int divisor;
    divisor = loadImage("project-paradox/sprites/divisor.png");
    moveImage(divisor, WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2, 0);
    drawImage(imageArray[divisor].texture, &imageArray[divisor].dest, 0);

    //========Player1========
    player player1;
    player1.frame[0] = loadImage("project-paradox/sprites/player1_idle.png");
    player1.frame[1] = loadImage("project-paradox/sprites/player1_move.png");
    player1.frame[2] = loadImage("project-paradox/sprites/player1_jump.png");
    player1.id = player1.frame[0];
    toggleActive(player1.frame[1], 0);
    toggleActive(player1.frame[2], 0);

    player1.x_vel = 0;
    player1.y_vel = 0;
    player1.x_pos = 0;
    player1.y_pos = 0;
    player1.width = imageArray[player1.id].dest.w;
    player1.height = imageArray[player1.id].dest.h;
    player1.flip = 0;
    moveImage(player1.id, player1.x_pos, player1.y_pos);
    drawImage(imageArray[player1.id].texture, &imageArray[player1.id].dest, player1.flip);

    //========Player2========
    player player2;
    player2.frame[0] = loadImage("project-paradox/sprites/player2_idle.png");
    player2.frame[1] = loadImage("project-paradox/sprites/player2_move.png");
    player2.frame[2] = loadImage("project-paradox/sprites/player2_jump.png");
    player2.id = player2.frame[0];
    toggleActive(player2.frame[1], 0);
    toggleActive(player2.frame[2], 0);

    player2.x_vel = 0;
    player2.y_vel = 0;
    player2.x_pos = WINDOW_WIDTH;
    player2.y_pos = 0;
    player2.width = imageArray[player2.id].dest.w;
    player2.height = imageArray[player2.id].dest.h;
    player2.flip = 0;
    moveImage(player2.id, player2.x_pos, player2.y_pos);
    //drawAllImages();
    drawImage(imageArray[player2.id].texture, &imageArray[player2.id].dest, player2.flip);


    /*
    int test;
    test = loadImage("project-paradox/sprites/player1.png");
    moveImage(test, 0 , 0);
    drawImage(imageArray[test].texture, &imageArray[test].dest);
    */
    while (!close_requested) {
        //listeners 
        eventListener(P1ptr, P2ptr);

        //========Player1 Movement========
        player1.x_vel = player1.y_vel = 0;
        if (P1.up && !P1.down) {
            player1.id = player1.frame[2];
            toggleActive(player1.frame[1], 0);
            toggleActive(player1.frame[0], 0);
            toggleActive(player1.frame[2], 1);
            player1.y_vel = -SPEED;
        }
        if (P1.down && !P1.up) {
            player1.id = player1.frame[2];
            toggleActive(player1.frame[1], 0);
            toggleActive(player1.frame[0], 0);
            toggleActive(player1.frame[2], 1);
            player1.y_vel = SPEED;

            gameOutcome = 1;
            return gameOutcome;

        }
        if (P1.left && !P1.right) {
            player1.flip = 1;
            player1.id = player1.frame[1];
            toggleActive(player1.frame[2], 0);
            toggleActive(player1.frame[0], 0);
            toggleActive(player1.frame[1], 1);
            player1.x_vel = -SPEED;

            gameOutcome = 2;
            return gameOutcome;
        }
        if (P1.right && !P1.left) {
            player1.flip = 0;
            player1.id = player1.frame[1];
            toggleActive(player1.frame[2], 0);
            toggleActive(player1.frame[0], 0);
            toggleActive(player1.frame[1], 1);
            player1.x_vel = SPEED;
        }
        if (player1.x_vel == 0 && player1.y_vel == 0) {
            player1.id = player1.frame[0];
            toggleActive(player1.frame[2], 0);
            toggleActive(player1.frame[1], 0);
            toggleActive(player1.frame[0], 1);

        }

        //cambio de pos
        player1.x_pos += player1.x_vel / FRAMES;
        player1.y_pos += player1.y_vel / FRAMES;


        //========Player1 Collisions========
        if (player1.x_pos + player1.x_vel / FRAMES <= 0) {
            player1.x_pos = 0;
        }
        if (player1.y_pos + player1.y_vel / FRAMES <= 0) {
            player1.y_pos = 0;
        }
        if (player1.x_pos + player1.x_vel / FRAMES >= WINDOW_WIDTH - player1.width) {
            player1.x_pos = WINDOW_WIDTH - player1.width;
        }
        if (player1.y_pos + player1.y_vel / FRAMES >= WINDOW_HEIGHT - player1.height) {
            player1.y_pos = WINDOW_HEIGHT - player1.height;
        }

        //colission floor divisor
        if (player1.x_pos + player1.width >= imageArray[divisor].dest.x) {//COLISION PLAYER1 CON EL DIVISOR DESDE LA IZQUIERDA
            player1.x_pos = (WINDOW_WIDTH / 2 - imageArray[divisor].dest.w / 2) - player1.width;
        }

        if (player1.y_pos + player1.height >= WINDOW_HEIGHT - imageArray[floor].dest.h) {//COLISION PLAYER1 CON EL SUELO
            player1.y_pos = (WINDOW_HEIGHT - imageArray[floor].dest.h) - player1.height;
        }
        moveImage(player1.id, player1.x_pos, player1.y_pos);
        toggleFlip(player1.id, player1.flip);
        //========Player2 Movement========
        player2.x_vel = player2.y_vel = 0;
        if (P2.up && !P2.down) {
            player2.id = player2.frame[2];
            toggleActive(player2.frame[1], 0);
            toggleActive(player2.frame[0], 0);
            toggleActive(player2.frame[2], 1);
            player2.y_vel = -SPEED;
        }
        if (P2.down && !P2.up) {
            player2.id = player2.frame[2];
            toggleActive(player2.frame[1], 0);
            toggleActive(player2.frame[0], 0);
            toggleActive(player2.frame[2], 1);
            player2.y_vel = SPEED;

        }
        if (P2.left && !P2.right) {
            player2.flip = 1;
            player2.id = player2.frame[1];
            toggleActive(player2.frame[2], 0);
            toggleActive(player2.frame[0], 0);
            toggleActive(player2.frame[1], 1);

            player2.x_vel = -SPEED;
        }
        if (P2.right && !P2.left) {
            player2.flip = 0;
            player2.id = player2.frame[1];
            toggleActive(player2.frame[2], 0);
            toggleActive(player2.frame[0], 0);
            toggleActive(player2.frame[1], 1);
            player2.x_vel = SPEED;

        }
        if (player2.x_vel == 0 && player2.y_vel == 0) {
            player2.id = player2.frame[0];
            toggleActive(player2.frame[2], 0);
            toggleActive(player2.frame[1], 0);
            toggleActive(player2.frame[0], 1);

        }

        //cambio de pos
        player2.x_pos += player2.x_vel / FRAMES;
        player2.y_pos += player2.y_vel / FRAMES;


        //========Player2 Collisions========
        if (player2.x_pos + player2.x_vel / FRAMES <= 0) {
            player2.x_pos = 0;
        }
        if (player2.y_pos + player2.y_vel / FRAMES <= 0) {
            player2.y_pos = 0;
        }
        if (player2.x_pos + player2.x_vel / FRAMES >= WINDOW_WIDTH - player2.width) {
            player2.x_pos = WINDOW_WIDTH - player2.width;
        }
        if (player2.y_pos + player2.y_vel / FRAMES >= WINDOW_HEIGHT - player2.height) {
            player2.y_pos = WINDOW_HEIGHT - player2.height;
        }

        //colission floor divisor
        if (player2.x_pos <= imageArray[divisor].dest.x + imageArray[divisor].dest.w) {//COLISION PLAYER1 CON EL DIVISOR DESDE LA IZQUIERDA
            player2.x_pos = (WINDOW_WIDTH / 2 + imageArray[divisor].dest.w / 2);
        }

        if (player2.y_pos + player2.height >= WINDOW_HEIGHT - imageArray[floor].dest.h) {//COLISION PLAYER2 CON EL SUELO
            player2.y_pos = (WINDOW_HEIGHT - imageArray[floor].dest.h) - player2.height;
        }
        moveImage(player2.id, player2.x_pos, player2.y_pos);
        toggleFlip(player2.id, player2.flip);
        /*
        //limpiamos la ventana
        SDL_RenderClear(gRenderer);
        */
        //y_pos -= (float)SCROLL_SPEED / FRAMES;
        //drawImage(imageArray[player1.id].texture, &imageArray[player1.id].dest, 0);
        //drawImage(imageArray[player2.id].texture, &imageArray[player2.id].dest, 0);



        drawAllImages();
        updateWindow();
        clearWindow();


        //dibujamos la imagen en la ventana
        //con que queremos renderizarlo, la textura, posicion en el spritesheet y destino
        // SDL_RenderCopy(gRenderer, texture, NULL, &destination);
        //SDL_RenderPresent(gRenderer);



        //esperamos para conseguir el framerate que queremos
        SDL_Delay(1000 / FRAMES);
    }

}